﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Interaction.h"


//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UInteractionComponent----------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UInteractionComponent::UInteractionComponent()
{
	Modules.SetNum(ModuleCounter);
	for(auto& Elem:Modules)
	{
		Elem = nullptr;
	}
	if(const auto Interface = GetOwner<IInteractionInterface>())
	{
		Interface->Init(this);
	}
}

void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
}
