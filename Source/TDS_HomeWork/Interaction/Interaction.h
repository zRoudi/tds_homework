﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"

// Interfaces

// Generated
#include "Interaction.generated.h"

#define GENERATED_MODULE_BODY()\
	friend class UInteractionComponent;\
	static inline const uint8 ModuleID = UInteractionComponent::GenerateModuleID();\
	UInteractionComponent* InteractionComponent = nullptr;

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UInteractionBase---------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UInteraction : public UObject
{
	GENERATED_BODY()
public: // Public functions

public: // Public variables

protected: // Protected functions

protected: // Protected variables

private: // Private functions

private: // Private variables
};

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------IInteractionInterface----------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UINTERFACE()
class UInteractionInterface : public UInterface
{
	GENERATED_BODY()
};
class TDS_HOMEWORK_API IInteractionInterface
{
	GENERATED_BODY()
	friend class UInteractionComponent;
public:
	

	UInteractionComponent* Get() const
	{
		return Component;
	}
	
private:
	
	struct TPointer
	{
	public:
		TPointer(AActor* ModuleOwner):Interface(Cast<IInteractionInterface>(ModuleOwner)){}
		explicit operator bool() const
		{
			return Interface && Interface->Component;
		}
		operator UInteractionComponent*() const
		{
			return Interface->Component;
		}
		UInteractionComponent* operator->() const
		{
			return Interface->Component;
		}
		UInteractionComponent& operator*() const
		{
			return *Interface->Component;
		}
	private:
		IInteractionInterface* const Interface;
	};
	
	void Init(UInteractionComponent* Component_){Component = Component_;}
	
	// ReSharper disable once CppUE4ProbableMemoryIssuesWithUObject
	UInteractionComponent* Component = nullptr;
	
};

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UInteractionComponent----------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(BlueprintType,Blueprintable,meta = (BlueprintSpawnableComponent),HideCategories = ("ComponentTick", "Variable", "Tags", "Activation", "Cooking", "AssetUserData", "Collision"))
class TDS_HOMEWORK_API UInteractionComponent : public UActorComponent
{
	GENERATED_BODY()
	static inline uint8 ModuleCounter = 0u;
public: // Public functions

	UInteractionComponent();
	virtual void BeginPlay() override;
	
	static uint8 GenerateModuleID()
	{
		return ModuleCounter++;
	}
	/**
	 * @brief 
	 * @param Module 
	 * @param ModuleOwner ->  Actor Owning InteractionComponent
	 */
	template<class ModuleType>
	static void RegisterModule(ModuleType* Module, AActor* ModuleOwner)
	{
		if(const IInteractionInterface::TPointer Pointer = ModuleOwner)
		{
			Pointer->AddModule(Module);
		}
	}
	template<class ModuleType, class FunctorType>
	static void RegisterModule(ModuleType* Module, AActor* ModuleOwner, FunctorType&& OnRegistrationEnded)
	{
		if(const IInteractionInterface::TPointer Pointer = ModuleOwner)
		{
			Pointer->AddModule(Module);
			Pointer->OnModulesRegistered.AddLambda(Forward<FunctorType>(OnRegistrationEnded));
		}
	}
	
	template<class ModuleType>
	static ModuleType* FindModule(AActor* ModuleOwner)
	{
		IInteractionInterface::TPointer Pointer = ModuleOwner;
		return Pointer ? Pointer->FindModule<ModuleType>() : nullptr;
	}
	
	template<class ModuleType>
	void AddModule(ModuleType* Module)
	{
		if(Modules[ModuleType::ModuleID])
		{
			ERROR("UInteractionComponent::AddModule Cannot add Simular modules");
			return;
		}
		Modules[ModuleType::ModuleID] = Module;
		Module->InteractionComponent = this;
	}
	
	template<class ModuleType>
	ModuleType*  FindModule()
	{
		return Cast<ModuleType>(Modules[ModuleType::ModuleID]);
	}

	void FinishModuleRegistration() const
	{
		OnModulesRegistered.Broadcast();
	}
	
public: // Public variables
	
protected: // Protected functions
	
protected: // Protected variables

	
	TMulticastDelegate<void()> OnModulesRegistered;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Interaction")
	TArray<UObject*> Modules;	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Interaction")
	TArray<UInteraction*> ActiveInteractions;
	
private: // Private variables

private: // Private functions
};