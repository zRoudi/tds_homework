﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Interfaces

// Generated
#include "AmmoInventory.generated.h"

USTRUCT(Blueprintable)
struct FAmmoSlot
{
	GENERATED_USTRUCT_BODY()
public:
	FNoParam OnChange;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ammo")
	int Ammo = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	int MaxAmount = 100;
	FORCEINLINE bool HasAmmo() const
	{
		return Ammo > 0;
	}
	FORCEINLINE bool IsMax() const
	{
		return Ammo == MaxAmount;
	}
	/**
	* @return Returns amount of ammo (which has)
	*/
	FORCEINLINE int GetAvailableAmmo(const int& Amount) const
	{
		return FMath::Min(Ammo, Amount);
	}
	/**
	* @return Returns amount of ammo (which has)
	*/
	FORCEINLINE int GetAvailableAmmo() const
	{
		return Ammo;
	}
	/**
	* @return Returns amount removed from inventory
	*/
	FORCEINLINE int RemoveAmmo(int Amount)
	{
		Amount = GetAvailableAmmo(Amount);
		Ammo -= Amount;
		OnChange.Broadcast();
		return Amount;
	}
	FORCEINLINE void AddAmmo(const int& Amount)
	{
		Ammo += Amount;
		if (Ammo > MaxAmount) {
			Ammo = MaxAmount;
		}
		OnChange.Broadcast();
	}
};

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UAmmoInventoryModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()

public: // Public functions

	UAmmoInventoryModule();
	
	UFUNCTION(BlueprintCallable)
	void AddAmmo(EWeaponType AmmoType, int Amount);
	UFUNCTION(BlueprintCallable)
	void RemoveAmmo(EWeaponType AmmoType, int Amount);
	UFUNCTION(BlueprintCallable)
	int GetAmmo(EWeaponType Type);
	UFUNCTION(BlueprintCallable)
	bool IsMax(EWeaponType Type)
	{
		return AmmoSlots[Type].IsMax();
	}
	
public: // Public variables
	
protected: // Protected functions

protected: // Protected variables

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Slots")
	TMap<TEnumAsByte<EWeaponType>, FAmmoSlot> AmmoSlots;
	
private: // Private functions

private: // Private variables
};

