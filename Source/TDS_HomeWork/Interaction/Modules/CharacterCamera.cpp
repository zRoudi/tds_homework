﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterCamera.h"

#include "InputBindings.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"


// Sets default values
UCharacterCameraModule::UCharacterCameraModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]
		{
			if(const auto InputBindingsModule = InteractionComponent->FindModule<UInputBindingsModule>())
			{
				InputBindingsModule->D_OnGamePaused.AddDynamic(this, &UCharacterCameraModule::OnGamePaused);
				InputBindingsModule->Add("ZoomIn", this, IE_Pressed, &UCharacterCameraModule::I_ZoomIn);
				InputBindingsModule->Add("ZoomOut", this, IE_Pressed, &UCharacterCameraModule::I_ZoomOut);
			}
		}
	);
	PrimaryComponentTick.bCanEverTick = false;
	
	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(this);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->SetRelativeRotation({-90.f, 0.f, 0.f});
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;
}

// Called when the game starts or when spawned
void UCharacterCameraModule::BeginPlay()
{
	Super::BeginPlay();
	CameraBoom->TargetArmLength = MinZoom;
}

void UCharacterCameraModule::Aim() const
{
	
	CameraBoom->SetRelativeLocation(FVector(AnimDistance, 0, 0));
}

void UCharacterCameraModule::Reset() const
{
	CameraBoom->SetRelativeLocation(FVector::ZeroVector);
}

void UCharacterCameraModule::OnGamePaused()
{
	
}
