﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Components/WidgetComponent.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Interfaces


// Generated
#include "HealthSystem.generated.h"



//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHS_BarWidget-----------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(Blueprintable)
class TDS_HOMEWORK_API UHS_BarWidget : public UUserWidget
{
	GENERATED_BODY()
	friend class UHS_MainWidget;
public: // Public functions
	
public: // Public variables

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UProgressBar* PB_State;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TB_State;
	
protected: // Protected functions
	
	UFUNCTION()
	void OnFull();
	UFUNCTION()
	void OnEmpty();
	UFUNCTION()
	void OnChange();

protected: // Protected variables

private: // Private functions

	void Bind(class UHPBar* Bar_);
	
private: // Private variables

	class UHPBar* Bar = nullptr;
	
};

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHS_MainWidget----------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(Blueprintable)
class TDS_HOMEWORK_API UHS_MainWidget : public UUserWidget
{
	GENERATED_BODY()
	friend class UHealthSystemModule;
public: // Public functions

public: // Public variables
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UPanelWidget* BarContainer;
	
protected: // Protected functions

protected: // Protected variables

private: // Private functions
	
	void Init(class UHealthSystemModule* Component_);;

private: // Private variables

	class UHealthSystemModule* Component = nullptr;
	
};

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHS_HitResponse----------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UHS_HitResponse : public UDataAsset
{
	GENERATED_BODY()

public: // Public functions

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Decal")
	TArray<UMaterialInterface*> DecalMaterials;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Decal")
	float DecalLifeTime;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Decal")
	FVector DecalSize;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Particles")
	TArray<UParticleSystem*> Particles;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Sound")
	TArray<USoundBase*> Sounds;
	
public: // Public variables

protected: // Protected functions

protected: // Protected variables

private: // Private functions

private: // Private variables
};

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHPBar-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(Blueprintable)
class TDS_HOMEWORK_API UHPBar : public UActorComponent
{
	GENERATED_BODY()
	friend class UHealthSystemModule;
	friend class UHS_BarWidget;
public: // Public functions

	UHPBar();
	virtual void BeginPlay() override;
	virtual void TickComponent(float, ELevelTick, FActorComponentTickFunction*) override;
	
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsFull() const { return CurrentCapacity == MaxCapacity; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsEmpty() const { return CurrentCapacity == 0; }
	UFUNCTION(BlueprintCallable)
	virtual int ApplyDamage(int Amount);
	UFUNCTION(BlueprintCallable)
	virtual int RestoreCapacity(int Amount);

public: // Public variables
	
	UPROPERTY(EditDefaultsOnly, BlueprintAssignable)
	FNoParam OnEmpty;
	UPROPERTY(EditDefaultsOnly, BlueprintAssignable)
	FNoParam OnFull;
	UPROPERTY(EditDefaultsOnly, BlueprintAssignable)
	FNoParam OnChange;
	
protected: // Protected functions

protected: // Protected variables
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget")
	FColor BarColor = FColor::Red;
	
private: // Private functions
	
	void Init(const struct FHPBarParmas& Params);;
	

private: // Private variables
	
	UPROPERTY(VisibleAnywhere,Category = "Capacity")
	int CurrentCapacity = 0;
	UPROPERTY(VisibleAnywhere,Category = "Capacity")
	int MaxCapacity = 0;
	UPROPERTY(VisibleAnywhere,Category = "Recoil")
	int RecoilAmount = 0;
	UPROPERTY(VisibleAnywhere,Category = "Recoil")
	float RecoilRate = 0;
	UPROPERTY(VisibleAnywhere,Category = "Recoil")
	float RecoilCooldown = 0;
	UPROPERTY(VisibleAnywhere,Category = "Recoil")
	FTimerHandle RecoilTimerHandle;
	UPROPERTY(VisibleAnywhere,Category = "HitResponse")
	const UHS_HitResponse* HitResponse;
};

USTRUCT(BlueprintType)
struct FHPBarParmas
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Class")
	TSubclassOf<UHPBar> BarClass;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Capacity", meta = (ClampMin = "0"))
	int StartCapacity = 0;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Capacity", meta = (ClampMin = "0"))
	int MaxCapacity = 100;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Recoil", meta = (ClampMin = "0"))
	int RecoilAmount = 1;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Recoil", meta = (ClampMin = "0"))
	float RecoilRate = 0.1;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Recoil", meta = (ClampMin = "0"))
	float RecoilCooldown = 1;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "HitResponse")
	const UHS_HitResponse* HitResponse;
};

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHealthSystemModule---------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(Blueprintable,meta = (BlueprintSpawnableComponent))
class TDS_HOMEWORK_API UHealthSystemModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()
	friend class UHS_MainWidget;
public:	// Public functions
	
	UHealthSystemModule();
	virtual void BeginPlay() override;
	virtual void OnConstruction();
	bool RestoreCapacity(int Amount);
	
public: // Public variables
	
	UPROPERTY(BlueprintAssignable)
	FNoParam OnDead;
	
protected: // Protected functions
	UFUNCTION()
	void OnTakeAnyDamage( AActor* DamagedActor, float Damage, const UDamageType* DamageType,
		AController* InstigatedBy, AActor* DamageCauser );

protected: // Protected variables
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget")
	bool bHaseWidget = true;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget")
	USceneComponent* WidgetAttachmentPoint;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget")
	UWidgetComponent* Widget;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget",meta = (EditCondition = "bHaseWidget", EditConditionHides))
	FVector WidgetOffset = FVector::ZeroVector;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget",meta = (EditCondition = "bHaseWidget", EditConditionHides))
	TSubclassOf<UHS_MainWidget> MainWidgetClass;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget",meta = (EditCondition = "bHaseWidget", EditConditionHides))
	TSubclassOf<UHS_BarWidget> BarWidgetClass;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Params")
	TArray<FHPBarParmas> BarsParams;
	UPROPERTY()
	TArray<UHPBar*> Bars;
	
	UPROPERTY()
	UBillboardComponent* BillboardComponent;
	
private: // Private Functions
	
private: // Private variables
	
};