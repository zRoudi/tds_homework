﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InputBindings.h"

#include "Kismet/GameplayStatics.h"

UInputBindingsModule::UInputBindingsModule()
{
	UInteractionComponent::RegisterModule(this,GetOwner());
	PrimaryComponentTick.bCanEverTick = false;
	Pawn = GetOwner<APawn>();
}

void UInputBindingsModule::BeginPlay()
{
	Super::BeginPlay();
	if(Pawn)
	{
		InputComponent = Pawn->InputComponent;
	}
	Bind();
}

void UInputBindingsModule::PauseGame()
{
	if(bIsPaused) return;
	bIsPaused = true;
	Unbind();
	UGameplayStatics::SetGlobalTimeDilation(this, 0.1f);
	D_OnGamePaused.Broadcast();
}

void UInputBindingsModule::UnpauseGame()
{
	if(!bIsPaused) return;
	bIsPaused = false;
	Bind();
	UGameplayStatics::SetGlobalTimeDilation(this, 1.0f);
	D_OnGameUnpaused.Broadcast();
}
