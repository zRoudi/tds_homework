﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Modules

// Generated
#include "InputBindings.generated.h"


DECLARE_DELEGATE_OneParam(FActionWithBool, bool);
DECLARE_DELEGATE_OneParam(FActionWithInt, int);

class UActionBase
{
	friend class UInputBindingsModule;
public:
	virtual ~UActionBase() = default;
	UActionBase(const FName& ActionName_, UInputComponent*& InputComponent_)
		:ActionName(ActionName_)
		,InputComponent(InputComponent_)
	{}
	
	virtual void Bind() = 0;
	virtual void Unbind() = 0;
	FName ActionName;
	UInputComponent*& InputComponent;
};

template<typename ObjectType>
class UAction: public UActionBase
{
	friend class UInputBindingsModule;
	using Super = UActionBase;
public:
	virtual ~UAction() override = default;
	UAction(const FName& p1, UInputComponent*& p2, ObjectType* p3, EInputEvent p4, void (ObjectType::*p5)(void))
		: Super(p1,p2)
		, Object(p3)
		, Type(p4)
		, Func(p5)
	{}

	virtual void Bind() override
	{
		if(InputComponent && Func && Object)
			Handle = InputComponent->BindAction(ActionName,Type,Object,Func).GetHandle();
	}
	virtual void Unbind() override
	{
		if(InputComponent)
			InputComponent->RemoveActionBindingForHandle(Handle);
		Handle = 0;
	}

private:
	int32 Handle = 0;
	ObjectType* Object;
	EInputEvent Type;
	void (ObjectType::*Func)(void);
};

template<typename ObjectType>
class UActionBool final : public UActionBase
{
	friend class UInputBindingsModule;
	using Super = UActionBase;
public:
	
	virtual ~UActionBool() override = default;
	UActionBool(const FName& p1, UInputComponent*& p2, ObjectType* p3, void (ObjectType::*p4)(bool))
		: Super(p1,p2)
		, Object(p3)
		, Func(p4)
	{}
	
	virtual void Bind() override
	{
		if(InputComponent && Func && Object)
		{
			Handle1 = InputComponent->BindAction<FActionWithBool>(
				ActionName,IE_Pressed, Object, Func, true).GetHandle();
			Handle2 = InputComponent->BindAction<FActionWithBool>(
				ActionName,IE_Released, Object, Func, false).GetHandle();
		}
	}

	virtual void Unbind() override
	{
		if(InputComponent)
		{
			InputComponent->RemoveActionBindingForHandle(Handle1);
			InputComponent->RemoveActionBindingForHandle(Handle2);
		}
		Handle1 = 0;
		Handle2 = 0;
	}
	
private:
	int32 Handle1 = 0, Handle2 = 0;
	ObjectType* Object;
	void (ObjectType::*Func)(bool);
};

template<typename ObjectType>
class UAxis final : public UActionBase
{
	friend class UInputBindingsModule;
	using Super = UActionBase;
public:
	virtual ~UAxis() override = default;
	UAxis(const FName& p1, UInputComponent*& p2, ObjectType* p3, void (ObjectType::*p4)(float))
		: Super(p1,p2)
		, Object(p3)
		, Func(p4)
	{}
	virtual void Bind() override
	{
		if(InputComponent && Func && Object)
		{
			if(Handle)
			{
				Handle->AxisDelegate.BindDelegate(Object,Func);
			}
			else
			{
				Handle = &InputComponent->BindAxis(ActionName, Object, Func);
			}
		}
	}

	virtual void Unbind() override
	{
		if(Handle)
			Handle->AxisDelegate.Unbind();
	}
private:
	FInputAxisBinding* Handle = nullptr;
	ObjectType* Object;
	void (ObjectType::*Func)(float);
};

template<typename ObjectType>
class UActionInt final : public UActionBase
{
	friend class UInputBindingsModule;
	using Super = UActionBase;
public:
	virtual ~UActionInt() override = default;
	UActionInt(const FName& p1, UInputComponent*& p2, ObjectType* p3, void (ObjectType::*p4)(int), int p5)
		: Super(p1,p2)
		, Object(p3)
		, Func(p4)
		, Value(p5)
	{}

	
	virtual void Bind() override
	{
		if(InputComponent && Func && Object)
			Handle = InputComponent->BindAction<FActionWithInt>(ActionName,IE_Pressed, Object, Func, Value).GetHandle();
	}
	virtual void Unbind() override
	{
		if(InputComponent)
			InputComponent->RemoveActionBindingForHandle(Handle);
		Handle = 0;
	}

private:
	int32 Handle = 0;
	ObjectType* Object;
	void (ObjectType::*Func)(int);
	int Value;
};

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UInputBindingsModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()

public: // Public functions

	UInputBindingsModule();
	virtual ~UInputBindingsModule() override
	{
		for(auto& Action:Actions)
		{
			if(Action)
			{
				delete Action;
				Action = nullptr;
			}
		}
	}

	virtual void BeginPlay() override;

	UInputComponent* GetInputComponent() const
	{
		return InputComponent;
	}

	template<typename ObjectType>
	void Add(const FName& N, EInputEvent IE,void (ObjectType::*F)(void))
	{
		Actions.Add(static_cast<UActionBase*>(new UAction(N, InputComponent, Pawn, IE, F)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, void (ObjectType::*F)(bool))
	{
		Actions.Add(static_cast<UActionBase*>(new UActionBool(N, InputComponent, Pawn, F)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, void (ObjectType::*F)(float))
	{
		Actions.Add(static_cast<UActionBase*>(new UAxis(N, InputComponent, Pawn, F)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, void (ObjectType::*F)(int),int V)
	{
		Actions.Add(static_cast<UActionBase*>(new UActionInt(N, InputComponent, Pawn, F, V)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, ObjectType* O, EInputEvent IE, void (ObjectType::*F)(void))
	{
		Actions.Add(static_cast<UActionBase*>(new UAction(N, InputComponent, O, IE, F)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, ObjectType* O, void (ObjectType::*F)(bool))
	{
		Actions.Add(static_cast<UActionBase*>(new UActionBool(N, InputComponent, O, F)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, ObjectType* O, void (ObjectType::*F)(float))
	{
		Actions.Add(static_cast<UActionBase*>(new UAxis(N, InputComponent, O, F)));
	}
	
	template<typename ObjectType>
	void Add(const FName& N, ObjectType* O, void (ObjectType::*F)(int),int V)
	{
		Actions.Add(static_cast<UActionBase*>(new UActionInt(N, InputComponent, O, F, V)));
	}

	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, EInputEvent IE,void (ObjectType::*F)(void))
	{
		InputComponent->BindAction(N, IE, Cast<ObjectType>(Pawn), F);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, void (ObjectType::*F)(bool))
	{
		InputComponent->BindAction<FActionWithBool>(N,IE_Pressed, Cast<ObjectType>(Pawn), F, true);
		InputComponent->BindAction<FActionWithBool>(N,IE_Released, Cast<ObjectType>(Pawn), F, false);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, void (ObjectType::*F)(float))
	{
		InputComponent->BindAxis(N, Cast<ObjectType>(Pawn), F);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, void (ObjectType::*F)(int),int V)
	{
		InputComponent->BindAction<FActionWithInt>(N,IE_Pressed, Cast<ObjectType>(Pawn), F, V);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, ObjectType* O, EInputEvent IE, void (ObjectType::*F)(void))
	{
		InputComponent->BindAction(N, IE, O, F);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, ObjectType* O, void (ObjectType::*F)(bool))
	{
		InputComponent->BindAction<FActionWithBool>(N,IE_Pressed, O, F, true);
		InputComponent->BindAction<FActionWithBool>(N,IE_Released, O, F, false);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, ObjectType* O, void (ObjectType::*F)(float))
	{
		InputComponent->BindAxis(N, O, F);
	}
	
	template<typename ObjectType>
	void AddAlwaysActive(const FName& N, ObjectType* O, void (ObjectType::*F)(int),int V)
	{
		InputComponent->BindAction<FActionWithInt>(N,IE_Pressed, O, F, V);
	}

	UFUNCTION()
	void PauseGame();
	UFUNCTION()
	void UnpauseGame();
	
public: // Public variables

	UPROPERTY()
	bool bIsPaused = false;	
	UPROPERTY(BlueprintAssignable)
	FNoParam D_OnGamePaused;
	UPROPERTY(BlueprintAssignable)
	FNoParam D_OnGameUnpaused;
	
protected: // Protected functions

	void Bind()
	{
		for(auto& Action: Actions)
		{
			if(Action)
			{
				Action->Bind();
			}
		}
	}
	void Unbind()
	{
		for(auto Action = Actions.CreateIterator(); Action; ++Action)
			if(*Action)
				(*Action)->Unbind();
	}
	
protected: // Protected variables

private: // Private functions

private: // Private variables
	
	APawn* Pawn;
	UInputComponent* InputComponent;
	TArray<UActionBase*> Actions;	
};
