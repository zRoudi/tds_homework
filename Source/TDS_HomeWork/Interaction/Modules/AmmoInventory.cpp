﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoInventory.h"

#include "WidgetModule.h"

UAmmoInventoryModule::UAmmoInventoryModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]
		{
			if(const auto WidgetModule = InteractionComponent->FindModule<UWidgetModule>())
			{
				WidgetModule->AddRequirement("AmmoInventoryWidget");
			}
		}
		);
	FillMap(AmmoSlots);
}

void UAmmoInventoryModule::AddAmmo(EWeaponType AmmoType, int Amount)
{
	AmmoSlots.Find(AmmoType)->AddAmmo(Amount);
}

void UAmmoInventoryModule::RemoveAmmo(EWeaponType AmmoType, int Amount)
{
	AmmoSlots.Find(AmmoType)->RemoveAmmo(Amount);
}

int UAmmoInventoryModule::GetAmmo(EWeaponType AmmoType)
{
	return AmmoSlots.Find(AmmoType)->Ammo;
}
