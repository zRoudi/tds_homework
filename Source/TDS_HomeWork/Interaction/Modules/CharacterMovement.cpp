﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterMovement.h"

#include "CharacterCamera.h"
#include "InputBindings.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS_HomeWork/TDS_HomeWork.h"
#include "TDS_HomeWork/Interaction/Interaction.h"
#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"


// Sets default values
UCharacterMovementModule::UCharacterMovementModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]
		{
			if(const auto InputBindingsModule = InteractionComponent->FindModule<UInputBindingsModule>())
			{
				InputBindingsModule->D_OnGamePaused.AddDynamic(this, &UCharacterMovementModule::OnGamePaused);
				InputBindingsModule->D_OnGameUnpaused.AddDynamic(this, &UCharacterMovementModule::OnGameUnpaused);
					
				InputBindingsModule->Add("Sprint", this, IE_Pressed, &UCharacterMovementModule::I_EnableSprint);
				InputBindingsModule->Add("Sprint", this, IE_Released, &UCharacterMovementModule::I_DisableSprint);
					
				InputBindingsModule->Add("MoveForward", this, &UCharacterMovementModule::I_AxisX);
				InputBindingsModule->Add("MoveRight", this, &UCharacterMovementModule::I_AxisY);
					
				InputBindingsModule->Add("Aim",this, IE_Pressed, &UCharacterMovementModule::I_EnableAim);
				InputBindingsModule->Add("Aim",this, IE_Released, &UCharacterMovementModule::I_DisableAim);
			}
		}
	);
	PrimaryComponentTick.bCanEverTick = true;
	
	FillMap(MovementSpeed);

	if(auto Character = GetOwner<ACharacter>())
	{
		MovementComponent = Character->GetCharacterMovement();
		if(!MovementComponent)
		{
			ERROR("UCharacterMovementModule::UCharacterMovementModule Owner hase no movement component");
			return;
		}
		MovementComponent->bOrientRotationToMovement = true; // Rotate character to moving direction
		MovementComponent->RotationRate = FRotator(0.f, 640.f, 0.f);
		MovementComponent->bConstrainToPlane = true;
		MovementComponent->bSnapToPlaneAtStart = true;
	}
}

void UCharacterMovementModule::BeginPlay()
{
	PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	WeaponInteraction = InteractionComponent->FindModule<UWeaponInteractionModule>();
	CharacterCamera = InteractionComponent->FindModule<UCharacterCameraModule>();
	if(WeaponInteraction && CharacterCamera)
	{
		WeaponInteraction->OnWeaponChange.AddDynamic(this, &UCharacterMovementModule::OnWeaponChanged);
		OnWeaponChanged();
	}else
	{
		SetRestrictions(EMovementRestriction::CannotAim);		
	}
	Super::BeginPlay();
}

void UCharacterMovementModule::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateWalkRequest();
	
	if(ToUpdateMovementState)
	{
		const auto OldState = MovementState;
		UpdateMovementState();
		if(OldState != MovementState)
		{
			MovementComponent->MaxWalkSpeed = MovementSpeed[MovementState];
			OnMovementStateChanged.Broadcast();
		}
		ToUpdateMovementState = false;
	}
	
	UpdateRotation();
	UpdateLocation();
}

void UCharacterMovementModule::OnWeaponChanged()
{
	if(WeaponInteraction->GetWeapon())
	{
		const auto& Info = WeaponInteraction->GetWeapon()->GetInfo();
		if(Info.HaseAimMode)
		{
			RemoveRestrictions(EMovementRestriction::CannotAim);
			CharacterCamera->AnimDistance = Info.AimDistance;
		}else
		{
			SetRestrictions(EMovementRestriction::CannotAim);
		}
	}else
	{
		SetRestrictions(EMovementRestriction::CannotAim);
	}
}

void UCharacterMovementModule::UpdateRotation()
{
	if (PlayerController) {
		PlayerController->GetHitResultUnderCursor(ECC_Cursor, true, LastCursorHitResult);
		const float Yaw = UKismetMathLibrary::FindLookAtRotation(GetOwner()->GetActorLocation(), LastCursorHitResult.Location).Yaw;
		GetOwner()->SetActorRotation({0.0f, Yaw, 0.0f});
	}
}

void UCharacterMovementModule::UpdateLocation()
{
	using enum EMovementState;
	switch (MovementState)
	{
	case Sprint_State:
		MovementComponent->AddInputVector(GetOwner()->GetActorForwardVector());
		return;
	case Aim_State:
		if(!HasRestriction(EMovementRestriction::CannotWalk))
			MovementComponent->AddInputVector({AxisX, AxisY, 0});
		CharacterCamera->Aim();
		return;
	case Walk_State:
		MovementComponent->AddInputVector({AxisX, AxisY, 0});
		return;
	case Static_State:
		return;
	}
}

void UCharacterMovementModule::UpdateMovementState()
{
	if(HasRequest(EMovementRequest::JumpRequest) && !HasRestriction(EMovementRestriction::CannotJump))
	{
		// TODO Jump
		return;	
	}
	if(HasRequest(EMovementRequest::AimRequest) && !HasRestriction(EMovementRestriction::CannotAim))
	{
		MovementState = EMovementState::Aim_State;
		return;
	}
	if(HasRestriction(EMovementRestriction::CannotWalk))
	{
		MovementState = EMovementState::Static_State;
		return;
	}	
	if(HasRequest(EMovementRequest::SprintRequest) && !HasRestriction(EMovementRestriction::CannotSprint))
	{
		MovementState = EMovementState::Sprint_State;
		return;
	}
	if(HasRequest(EMovementRequest::WalkRequest))
	{
		MovementState = EMovementState::Walk_State;
		return;
	}
	MovementState = EMovementState::Static_State;
}

void UCharacterMovementModule::UpdateWalkRequest()
{
	const auto OldRequests = MovementRequests;
	if(!FMath::IsNearlyZero(AxisX) || !FMath::IsNearlyZero(AxisY))
	{
		MovementRequests |= static_cast<uint32>(EMovementRequest::WalkRequest);
	}else
	{
		MovementRequests &= static_cast<uint32>(~EMovementRequest::WalkRequest);
	}
	ToUpdateMovementState |= (OldRequests != MovementRequests);
}

void UCharacterMovementModule::OnGamePaused()
{
	PrimaryComponentTick.SetTickFunctionEnable(false);
}

void UCharacterMovementModule::OnGameUnpaused()
{
	PrimaryComponentTick.SetTickFunctionEnable(true);
}
