﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "GameFramework/SpringArmComponent.h"
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Modules

// Generated
#include "CharacterCamera.generated.h"

UCLASS(Blueprintable, BlueprintType)
class TDS_HOMEWORK_API UCharacterCameraModule : public USceneComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()
	friend class UCharacterMovementModule;

public: // Public functions

	UCharacterCameraModule();

	virtual void BeginPlay() override;
	
	UFUNCTION()
	void I_ZoomIn()
	{
		float TargetArmLength = CameraBoom->TargetArmLength - ZoomStep;
		CameraBoom->TargetArmLength = TargetArmLength > MinZoom ? TargetArmLength : MinZoom;
	}
	UFUNCTION()
	void I_ZoomOut()
	{
		float TargetArmLength = CameraBoom->TargetArmLength + ZoomStep;
		CameraBoom->TargetArmLength = TargetArmLength < MaxZoom ? TargetArmLength : MaxZoom;
	}

public: // Public variables

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	USpringArmComponent* CameraBoom;
	
protected: // Protected functions

	UFUNCTION()
	void Aim() const;
	UFUNCTION()
	void Reset() const;
	
protected: // Protected variables

	UPROPERTY()
	float AnimDistance = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zoom")
	float MinZoom = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zoom")
	float MaxZoom = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zoom")
	float ZoomStep = 50.0f;

private: // Private functions
	
	UFUNCTION()
	void OnGamePaused();
	
private: // Private variables
};
