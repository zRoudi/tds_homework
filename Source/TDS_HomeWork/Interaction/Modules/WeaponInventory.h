// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Core classes
#include "CoreMinimal.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Generated
#include "WeaponInventory.generated.h"

// This class does not need to be modified.


class AWeaponBase;
USTRUCT(BlueprintType)
struct FWeaponSlot 
{
	GENERATED_USTRUCT_BODY()
	friend class UWeaponInventoryModule;
protected:

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	AWeaponBase* Weapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool isLocked = true;
	
	void Unlock()
	{
		isLocked = false;
		OnChange.Broadcast();
	}
	
public:
	
	void SetWeapon(AWeaponBase* NewWeapon)
	{
		Weapon = NewWeapon;
		OnChange.Broadcast();
	}
	AWeaponBase* GetWeapon() const
	{
		return Weapon;
	}
	
	UPROPERTY(BlueprintAssignable)
	FNoParam OnChange;
};

UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UWeaponInventoryModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()
	friend class UWeaponInteractionModule;

public: // Public functions
	
	UWeaponInventoryModule();
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable)
	AWeaponBase* GetCurrentWeapon()
	{
		return WeaponSlots[CurrentWeaponSlot].Weapon;
	}
	UFUNCTION(BlueprintCallable)
	FWeaponSlot& GetCurrentSlot()
	{
		return WeaponSlots[CurrentWeaponSlot];
	}
	UFUNCTION(BlueprintCallable)
	TArray<FWeaponSlot>& GetAllSlots();
	UFUNCTION(BlueprintCallable)
	bool AddWeapon(AWeaponBase* Weapon);
	UFUNCTION(BlueprintCallable)
	void ReplaceWeapon(AWeaponBase* Weapon);
	
public: // Public variables

	UPROPERTY(BlueprintAssignable)
	FNoParam OnCurrentSlotChange;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnCurrentWeaponChange;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Attachment")
	USceneComponent* WeaponAttachmentComponent;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Attachment")
	FName SocketName;
	
protected: // Protected functions

	void AttachWeapon(AWeaponBase* Weapon);
	UFUNCTION()
	void InputSwitchSlot(int SlotNum);

protected: // Protected variables

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category = "Slots")
	TArray<FWeaponSlot> WeaponSlots;

private: // Private functions
	
private: // Private variables

	class UWeaponInteractionModule* WeaponInteractionModule;
	UPROPERTY()
	int CurrentWeaponSlot = 0;
	
};