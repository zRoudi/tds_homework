﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"

#include "InputBindings.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDS_HomeWork/TDS_HomeWork.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Particles/ParticleSystemComponent.h"


//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UPickupModule---------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UPickupModule::UPickupModule()
{
	UInteractionComponent::RegisterModule(this,GetOwner());
	BillboardComponent = CreateDefaultSubobject<UBillboardComponent>("Billboard");
	BillboardComponent->SetMobility(EComponentMobility::Movable);
	if(GetOwner())
	{
		BillboardComponent->SetupAttachment(GetOwner()->GetRootComponent());
	}
	// PickupFX setup
	PickupFX = CreateDefaultSubobject<UParticleSystemComponent>("PickupFX");
	PickupFX->SetVisibility(false);
	PickupFX->SetMobility(EComponentMobility::Movable);
	PickupFX->SetUsingAbsoluteRotation(true);
	PickupFX->SetActive(false);
	PickupFX->SetupAttachment(BillboardComponent);
	PickupFX->SetCollisionProfileName("IgnoreAll");
	
	// PawnDetectionSphere setup
	PawnDetectionSphere = CreateDefaultSubobject<USphereComponent>("PawnDetectionSphere");
#ifdef WITH_EDITOR
	PawnDetectionSphere->SetVisibility(true);
	PawnDetectionSphere->SetHiddenInGame(false);
#else
	PawnDetectionSphere->SetVisibility(false);
#endif
	PawnDetectionSphere->SetMobility(EComponentMobility::Movable);
	PawnDetectionSphere->SetUsingAbsoluteRotation(true);
	PawnDetectionSphere->SetGenerateOverlapEvents(true);
	PawnDetectionSphere->SetupAttachment(BillboardComponent);
	PawnDetectionSphere->SetCollisionProfileName("PickupArea");

	// CursorDetectionSphere setup
	CursorDetectionSphere = CreateDefaultSubobject<USphereComponent>("CursorDetectionSphere");
#ifdef WITH_EDITOR
	CursorDetectionSphere->SetVisibility(true);
	CursorDetectionSphere->SetHiddenInGame(false);
#else
	CursorDetectionSphere->SetVisibility(false);
#endif
	CursorDetectionSphere->SetMobility(EComponentMobility::Movable);
	CursorDetectionSphere->SetUsingAbsoluteRotation(true);	
	CursorDetectionSphere->SetGenerateOverlapEvents(false);
	CursorDetectionSphere->SetupAttachment(BillboardComponent);
	CursorDetectionSphere->SetCollisionProfileName("IgnoreAll");
	CursorDetectionSphere->SetCollisionResponseToChannel(ECC_Cursor,ECR_Block);

	// PickupWidget setup
	PickupWidget = CreateDefaultSubobject<UWidgetComponent>("PickupWidget");
	PickupWidget->SetVisibility(false);
	PickupWidget->SetMobility(EComponentMobility::Movable);
	PickupWidget->SetUsingAbsoluteRotation(true);
	PickupWidget->SetWidgetSpace(EWidgetSpace::Screen);
	PickupWidget->SetDrawSize({100,100});
	PickupWidget->SetupAttachment(BillboardComponent);
	PickupWidget->SetCollisionProfileName("IgnoreAll");
}

void UPickupModule::BeginPlay()
{
	Super::BeginPlay();
	if(!PickupBodyMesh)
	{
		ERROR("IPickupInterface::Init PickupBodyMesh ccomponent not bound");
		return;
	}
	
	PickupBodyMesh->SetGenerateOverlapEvents(false);
	PickupBodyMesh->bIgnoreRadialForce = true;
	PickupBodyMesh->bIgnoreRadialImpulse = true;
	PickupBodyMesh->SetMobility(EComponentMobility::Movable);
	PickupBodyMesh->SetCollisionProfileName("Droppable");
	PickupBodyMesh->SetUseCCD(true);
	
	PickupFX->AttachToComponent(PickupBodyMesh,FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	PickupFX->SetTemplate(ParticleSystem);
	PickupFX->SetRelativeRotation(FRotator::ZeroRotator);
	GetOwner()->AddInstanceComponent(PickupFX);
	if(bGenerateOverlap)
	{
		PawnDetectionSphere->SetSphereRadius(PawnDetectionRadius,false);	
		PawnDetectionSphere->AttachToComponent(PickupBodyMesh,FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		GetOwner()->AddInstanceComponent(PawnDetectionSphere);
	}else
	{
		PawnDetectionSphere->DestroyComponent();
	}
	
	if(bGenerateCursorOver)
	{
		CursorDetectionSphere->SetSphereRadius(CursorDetectionRadius,false);
		CursorDetectionSphere->AttachToComponent(PickupBodyMesh,FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		GetOwner()->AddInstanceComponent(CursorDetectionSphere);
		
		PickupWidget->SetWidgetClass(WidgetClass);
		PickupWidget->InitWidget();
		PickupWidget->AttachToComponent(PickupBodyMesh,FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		GetOwner()->AddInstanceComponent(PickupWidget);
	}else
	{
		CursorDetectionSphere->DestroyComponent();
		PickupWidget->DestroyComponent();
	}
}

void UPickupModule::Activate(bool bReset)
{
	PickupFX->SetVisibility(true);
	PickupFX->Activate(true);
	PickupBodyMesh->SetSimulatePhysics(true);
	
	if(bGenerateOverlap)
	{
		PawnDetectionSphere->Activate(true);
		PawnDetectionSphere->OnComponentBeginOverlap.AddDynamic(this,&UPickupModule::Overlap);
	}
	if(bGenerateCursorOver)
	{
		CursorDetectionSphere->Activate(true);
		CursorDetectionSphere->OnBeginCursorOver.AddDynamic(this,&UPickupModule::BeginCursorOver);
		CursorDetectionSphere->OnEndCursorOver.AddDynamic(this,&UPickupModule::EndCursorOver);
	}
	// PickupSound setup
	UGameplayStatics::SpawnSoundAtLocation(this,	DropSound, GetOwner()->GetActorLocation());
}

void UPickupModule::Deactivate()
{
	PickupFX->SetVisibility(false);
	PickupFX->Deactivate();
	PickupBodyMesh->SetSimulatePhysics(false);
	
	PickupWidget->SetVisibility(false);
	
	if(bGenerateOverlap)
	{
		PawnDetectionSphere->OnComponentBeginOverlap.RemoveAll(this);
	}
	if(bGenerateCursorOver)
	{
		CursorDetectionSphere->SetCollisionResponseToChannel(ECC_Cursor,ECR_Ignore);
		CursorDetectionSphere->OnBeginCursorOver.RemoveAll(this);
		CursorDetectionSphere->OnEndCursorOver.RemoveAll(this);
	}
	
	// PickupSound setup
	UGameplayStatics::SpawnSoundAtLocation(this,	PickupSound, GetOwner()->GetActorLocation());
}

template <class Widget>
Widget* UPickupModule::GetWidget()
{
	return PickupWidget ? Cast<Widget>(PickupWidget->GetWidget()) : nullptr;
}

void UPickupModule::Overlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	OnOverlap.ExecuteIfBound(OtherActor);
}

void UPickupModule::BeginCursorOver(UPrimitiveComponent* TouchedComponent)
{
	PickupWidget->SetVisibility(true);
	OnBeginCursorOver.ExecuteIfBound();
}

void UPickupModule::EndCursorOver(UPrimitiveComponent* TouchedComponent)
{
	PickupWidget->SetVisibility(false);
	OnEndCursorOver.ExecuteIfBound();
}

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UPickupInteractionModule-------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UPickupInteractionModule::UPickupInteractionModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]
		{
			if(const auto InputBindingsModule = InteractionComponent->FindModule<UInputBindingsModule>())
			{
				InputBindingsModule->Add("Interact",this, IE_Pressed, &UPickupInteractionModule::I_StartInteraction);
				InputBindingsModule->Add("Interact",this,IE_Released,&UPickupInteractionModule::I_EndInteraction);
			}
		}
	);
}

void UPickupInteractionModule::BeginPlay()
{
	Super::BeginPlay();
}

void UPickupInteractionModule::I_StartInteraction()
{
	AActor* OverlappedActor = nullptr;
	D_GetOverlappedActor.ExecuteIfBound(OverlappedActor);
	PickupModule = UInteractionComponent::FindModule<UPickupModule>(OverlappedActor);
	if(PickupModule)
	{
		GetWorld()->GetTimerManager().SetTimer(
			PickupTimerHandle,
			this,
			&UPickupInteractionModule::OnTimerEnd,
			0.5f
			);
	}
}

void UPickupInteractionModule::I_EndInteraction()
{
	if(PickupModule)
	{
		GetWorld()->GetTimerManager().ClearTimer(PickupTimerHandle);
		PickupModule->D_OnClicked.ExecuteIfBound(GetOwner());
		PickupModule = nullptr;
	}
}

void UPickupInteractionModule::OnTimerEnd()
{
	if(PickupModule)
	{
		PickupModule->D_OnClickedContinuous.ExecuteIfBound(GetOwner());
		PickupModule = nullptr;
	}
}	

