﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

#include "TDS_HomeWork/Functions/Types.h"

#include "Pickup.generated.h"

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UPickupModule---------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UPickupModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()
public: // Public functions
	
	UPickupModule();
	virtual void BeginPlay() override;
	virtual void Activate(bool bReset = false) override;
	virtual void Deactivate() override;
	
public: // Public variables
	
	UPROPERTY(VisibleDefaultsOnly,BlueprintReadWrite,Category = "Params")
	UPrimitiveComponent* PickupBodyMesh = nullptr;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Params")
	bool bGenerateCursorOver = false;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Params")
	bool bGenerateOverlap = false;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Params")
	float PawnDetectionRadius = 40.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Params")
	float CursorDetectionRadius = 40.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Params")
	float Range = 300.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Widget")
	TSubclassOf<UUserWidget> WidgetClass = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* ParticleSystem = nullptr;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Sound")
	USoundBase* PickupSound = nullptr;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Sound")
	USoundBase* DropSound = nullptr;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Billboard")
	UBillboardComponent* BillboardComponent;
	
	TDelegate<void(AActor*)> OnOverlap;
	TDelegate<void()> OnBeginCursorOver;
	TDelegate<void()> OnEndCursorOver;
	TDelegate<void(AActor*)> D_OnClicked;
	TDelegate<void(AActor*)> D_OnClickedContinuous;
	
protected: // Protected functions

	template<class Widget>
	Widget* GetWidget();

	UFUNCTION()
	void Overlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
	void BeginCursorOver( UPrimitiveComponent* TouchedComponent);
	UFUNCTION()
	void EndCursorOver( UPrimitiveComponent* TouchedComponent);
	
protected: // Protected variables
	
private: // Private functions

private: // Private variables

	bool Activated = false;
	bool Initialized = false;

	UPROPERTY()
	class UWidgetComponent* PickupWidget = nullptr;
	UPROPERTY()
	class USphereComponent* CursorDetectionSphere = nullptr;
	UPROPERTY()
	class USphereComponent* PawnDetectionSphere = nullptr;
	UPROPERTY()
	class UParticleSystemComponent* PickupFX = nullptr;
};


//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UPickupInteractionModule-------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UPickupInteractionModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()

public: // Public functions

	UPickupInteractionModule();
	virtual void BeginPlay() override;
	
public: // Public variables
	
	TDelegate<void(AActor*&)> D_GetOverlappedActor;

protected: // Protected functions

	UFUNCTION()
	void I_StartInteraction();
	UFUNCTION()
	void I_EndInteraction();
	UFUNCTION()
	void OnTimerEnd();

	FTimerHandle PickupTimerHandle;
	class UPickupModule* PickupModule = nullptr;
	
protected: // Protected variables

private: // Private functions

private: // Private variables
};
