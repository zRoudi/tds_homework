﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WidgetModule.h"

#include "Algo/Compare.h"
#include "Blueprint/WidgetTree.h"
#include "TDS_HomeWork/Character/TDS_HomeWorkCharacter.h"

UWidgetModule::UWidgetModule()
{
	UInteractionComponent::RegisterModule(this,GetOwner());
}

void UWidgetModule::BeginPlay()
{
	Super::BeginPlay();
	if(!*WidgetClass)
	{
		ERROR("UWidgetModule::BeginPlay WidgetClass not provided");
		return;
	}
	
	if (const auto WidgetBase = CreateWidget<UWidgetBase>(GetWorld(), WidgetClass,"WidgetBase"))
	{
		WidgetBase->FillSlots(this);
		WidgetBase->SetOwningPlayer(nullptr); // TODO
	}else
	{
		ERROR("UWidgetModule::BeginPlay WidgetCreationError")
	}
}

int32 UWidgetModule::GetRequirements(TSet<FName>& Result) const
{
	Result.Empty();
	return Slots.GetKeys(Result);
}

int32 UWidgetModule::GetRequirements(TSubclassOf<AActor> Class, TSet<FName>& Result)
{
	
	if(const auto WidgetModule = UInteractionComponent::FindModule<UWidgetModule>(Class.GetDefaultObject()))
	{
		return WidgetModule->GetRequirements(Result);
	}else
	{
		ERROR("Module not found");
		return 0;
	}
}

UWidgetBase::UWidgetBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

#if WITH_EDITORONLY_DATA
void UWidgetBase::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	if(PropertyChangedEvent.GetPropertyName() != FName{TEXT("Actor")}) return;

	UWidgetModule::GetRequirements(Actor, Requirements);
}
#endif

void UWidgetBase::FillSlots(UWidgetModule* Module) const
{
	if(!WidgetTree) return;
	if(!Module) return;
	TSet<FName> Names;
	Module->GetRequirements(Names);
	WidgetTree->ForEachWidget(
		[&](UWidget* Widget)
		{
			const FName Name = Widget->GetFName();
			if(Names.Contains(Name))
			{
				Module->Slots[Name] = Cast<UWidgetSlot>(Widget);
			}
		}
		);
}

UWidgetSlot::UWidgetSlot(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
}
