// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Interaction/Modules/WeaponInventory.h"

// Core classes

// My classes
#include "InputBindings.h"
#include "WeaponInteraction.h"
#include "WidgetModule.h"
#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"

UWeaponInventoryModule::UWeaponInventoryModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]
		{
			if(const auto WidgetModule = InteractionComponent->FindModule<UWidgetModule>())
			{
				WidgetModule->AddRequirement("WeaponInventoryWidget");
			}
			if(const auto InputBindingsModule = InteractionComponent->FindModule<UInputBindingsModule>())
			{
				InputBindingsModule->Add("1",this, &UWeaponInventoryModule::InputSwitchSlot, 0);
				InputBindingsModule->Add("2",this, &UWeaponInventoryModule::InputSwitchSlot, 1);
				InputBindingsModule->Add("3",this, &UWeaponInventoryModule::InputSwitchSlot, 2);
				InputBindingsModule->Add("4",this, &UWeaponInventoryModule::InputSwitchSlot, 3);
			}
			WeaponInteractionModule = InteractionComponent->FindModule<UWeaponInteractionModule>();
		}
		);
}

void UWeaponInventoryModule::BeginPlay()
{
	Super::BeginPlay();
	if(!WeaponSlots.Num())
	{
		ERROR("UWeaponInventoryModule::BeginPlay No weapon slots provided");
		DestroyComponent(true);
	}
}
bool UWeaponInventoryModule::AddWeapon(AWeaponBase* Weapon)
{
	DEBUG("UWeaponInventoryModule::AddWeapon %p", Weapon);
	if(!WeaponSlots[CurrentWeaponSlot].Weapon)
	{
		WeaponSlots[CurrentWeaponSlot].SetWeapon(Weapon);
		OnCurrentWeaponChange.Broadcast();
		AttachWeapon(Weapon);
		return true;
	}
	for (auto& WeaponSlot : WeaponSlots)
	{
		if(WeaponSlot.isLocked || WeaponSlot.Weapon) continue;
		
		WeaponSlot.SetWeapon(Weapon);
		AttachWeapon(Weapon);		
		return true;
	}
	return false;
}

void UWeaponInventoryModule::ReplaceWeapon(AWeaponBase* Weapon)
{
	if(!Weapon) return;
	AttachWeapon(Weapon);
	if(WeaponSlots[CurrentWeaponSlot].Weapon) 
	{
		if(!AddWeapon(WeaponSlots[CurrentWeaponSlot].Weapon))
		{
			if(WeaponInteractionModule)
			{
				WeaponInteractionModule->I_Drop(GetOwner()->GetActorForwardVector() * 5.0f);
			}else
			{
				WeaponSlots[CurrentWeaponSlot].Weapon->I_Drop(GetOwner()->GetActorForwardVector() * 5.0f);
			}
		}
	}
	WeaponSlots[CurrentWeaponSlot].SetWeapon(Weapon);
	OnCurrentWeaponChange.Broadcast();
}

void UWeaponInventoryModule::AttachWeapon(AWeaponBase* Weapon)
{
	DEBUG("UWeaponInventoryModule::AttachWeapon %p", Weapon);
	if (!Weapon) return;
	Weapon->AttachTo(WeaponAttachmentComponent, SocketName);
	Weapon->SetActorHiddenInGame(true);
}

TArray<FWeaponSlot>& UWeaponInventoryModule::GetAllSlots()
{
	return WeaponSlots;
}

void UWeaponInventoryModule::InputSwitchSlot(int SlotNum)
{
	SlotNum = FMath::Clamp(SlotNum, 0, WeaponSlots.Num()-1);
	
	if (SlotNum == CurrentWeaponSlot) return;
	if (WeaponSlots[SlotNum].isLocked) return;
	
	CurrentWeaponSlot = SlotNum;
	OnCurrentSlotChange.Broadcast();
	OnCurrentWeaponChange.Broadcast();
}
