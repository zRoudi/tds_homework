﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "Blueprint/UserWidget.h"
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Modules

// Generated
#include "WidgetModule.generated.h"

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UWidgetModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()
	friend class UWidgetBase;

public: // Public functions
	
	UWidgetModule();
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void AddRequirement(FName Requirement)
	{
		Slots.Add(Requirement);
	}
	int32 GetRequirements(TSet<FName>& Result) const;
	UFUNCTION(BlueprintCallable)
	static int32 GetRequirements(TSubclassOf<AActor> Class, TSet<FName>& Result);

	
public: // Public variables

	UPROPERTY(VisibleAnywhere)
	uint8 ModuleId = ModuleID;
	
protected: // Protected functions

protected: // Protected variables

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Settings")
	TSubclassOf<class UWidgetBase> WidgetClass;
	
private: // Private functions

private: // Private variables

	UPROPERTY(EditAnywhere, Category = "Settings");
	TMap<FName, class UWidgetSlot*> Slots;
	
};


UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UWidgetBase: public UUserWidget
{
	GENERATED_BODY()
	friend class UWidgetModule;

public: // Public functions

	UWidgetBase(const FObjectInitializer& ObjectInitializer);
#if WITH_EDITORONLY_DATA
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	UPROPERTY(EditAnywhere, Category = "Requirements")
	TSubclassOf<AActor> Actor;

	UPROPERTY(EditAnywhere, Category = "Requirements")
	TSet<FName> Requirements;
#endif
	
public: // Public variables

protected: // Protected functions

protected: // Protected variables
	
private: // Private functions

	UFUNCTION(BlueprintCallable)
	void FillSlots(UWidgetModule* Module) const;

private: // Private variables
};


UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UWidgetSlot : public UUserWidget
{
	GENERATED_BODY()
	friend class UWidgetModule;
	friend class UWidgetBase;

public: // Public functions

	UWidgetSlot(const FObjectInitializer& ObjectInitializer);

public: // Public variables

protected: // Protected functions

protected: // Protected variables

private: // Private functions
	
private: // Private variables

	UWidgetBase* Base = nullptr;
	
};
