﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "WeaponInteraction.h"
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Modules

// Generated
#include "CharacterMovement.generated.h"

UENUM(meta = (Bitflags, UseEnumValuesAsMaskValuesInEditor = "true"))
enum class EMovementRestriction : uint32
{
	NoRestriction	= (0) UMETA(Hidden),
	CannotWalk		= (1 << 0),
	CannotSprint	= (1 << 1),
	CannotAim		= (1 << 2),
	CannotJump		= (1 << 3),
};
ENUM_CLASS_FLAGS(EMovementRestriction);

UENUM(meta = (Bitflags, UseEnumValuesAsMaskValuesInEditor = "true"))
enum class EMovementRequest : uint32
{
	NoRequest		= (0) UMETA(Hidden),
	WalkRequest		= (1 << 0),
	SprintRequest	= (1 << 1),
	AimRequest		= (1 << 2),
	JumpRequest		= (1 << 3),
};
ENUM_CLASS_FLAGS(EMovementRequest);

UCLASS(Blueprintable)
class TDS_HOMEWORK_API UCharacterMovementModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()

public: // Public functions

	UCharacterMovementModule();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	EMovementState GetMovementState() const
	{
		return MovementState;
	}
	
	UFUNCTION(BlueprintCallable)
	void AddRestrictions(UPARAM(meta = (Bitmask, BitmaskEnum = "EMovementRestriction")) int32 Bitmask)
	{
		MovementRestrictions |= Bitmask;
		ToUpdateMovementState = true;
	}
	void AddRestrictions(EMovementRestriction Bitmask)
	{
		MovementRestrictions |= static_cast<int32>(Bitmask);
		ToUpdateMovementState = true;
	}
	
	UFUNCTION(BlueprintCallable)
	void RemoveRestrictions(UPARAM(meta = (Bitmask, BitmaskEnum = "EMovementRestriction")) int32 Bitmask)
	{
		MovementRestrictions &= ~Bitmask;
		ToUpdateMovementState = true;
	}
	void RemoveRestrictions(EMovementRestriction Bitmask)
	{
		MovementRestrictions &= ~static_cast<int32>(Bitmask);
		ToUpdateMovementState = true;
	}
	
	UFUNCTION(BlueprintCallable)
	void SetRestrictions(UPARAM(meta = (Bitmask, BitmaskEnum = "EMovementRestriction")) int32 Bitmask)
	{
		MovementRestrictions = Bitmask;
		ToUpdateMovementState = true;
	}
	void SetRestrictions(EMovementRestriction Bitmask)
	{
		MovementRestrictions = static_cast<int32>(Bitmask);
		ToUpdateMovementState = true;
	}

	bool HasRestriction(EMovementRestriction Restriction) const
	{
		return static_cast<uint32>(Restriction) & MovementRestrictions;
	}

	bool HasRequest(EMovementRequest Request) const
	{
		return static_cast<uint32>(Request) & MovementRequests;
	}
	
	UFUNCTION()
	void I_AxisX(float Value)
	{
		AxisX = Value;
	}
	UFUNCTION()
	void I_AxisY(float Value)
	{
		AxisY = Value;
	}
	UFUNCTION()
	void I_EnableSprint()
	{
		MovementRequests |= static_cast<uint32>(EMovementRequest::SprintRequest);
		ToUpdateMovementState = true;
	}
	UFUNCTION()
	void I_DisableSprint()
	{
		MovementRequests &= ~static_cast<uint32>(EMovementRequest::SprintRequest);
		ToUpdateMovementState = true;
	}
	UFUNCTION()
	void I_EnableAim()
	{
		MovementRequests |= static_cast<uint32>(EMovementRequest::AimRequest);
		ToUpdateMovementState = true;
	}
	UFUNCTION()
	void I_DisableAim()
	{
		MovementRequests &= ~static_cast<uint32>(EMovementRequest::AimRequest);
		ToUpdateMovementState = true;
	}
	
public: // Public variables

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FNoParam OnMovementStateChanged;
	UPROPERTY()
	FHitResult LastCursorHitResult;
	
protected: // Protected functions

	UFUNCTION()
	void OnWeaponChanged();

	UFUNCTION()
	void UpdateRotation();
	UFUNCTION()
	void UpdateLocation();
	UFUNCTION()
	void UpdateMovementState();
	UFUNCTION()
	void UpdateWalkRequest();
	
	UFUNCTION()
	void OnGamePaused();
	UFUNCTION()
	void OnGameUnpaused();
	
protected: // Protected variables
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Bitmask,BitmaskEnum = "EMovementRestriction"),Category = "Movement")
	int32 MovementRestrictions = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Bitmask,BitmaskEnum = "EMovementRequest"),Category = "Movement")
	int32 MovementRequests = 0;
	
	UPROPERTY(EditDefaultsOnly, EditFixedSize, BlueprintReadOnly, Category = "Speed")
	TMap<TEnumAsByte<EMovementState>, float> MovementSpeed;
	
private: // Private functions
	
private: // Private variables

	UPROPERTY()
	bool ToUpdateMovementState = false;
	
	UPROPERTY()
	EMovementState MovementState = EMovementState::Static_State;
	UPROPERTY()
	class UCharacterMovementComponent* MovementComponent;
	UPROPERTY()
	APlayerController* PlayerController;
	UPROPERTY()
	class UWeaponInteractionModule* WeaponInteraction;
	UPROPERTY()
	class UCharacterCameraModule* CharacterCamera;
	
	UPROPERTY()
	float AxisX = 0.0f;
	UPROPERTY()
	float AxisY = 0.0f;
};