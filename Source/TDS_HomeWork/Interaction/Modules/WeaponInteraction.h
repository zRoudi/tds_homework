﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Interfaces

// Generated
#include "WeaponInteraction.generated.h"

/**
 * requires WeaponInventoryModule, AmmoInventoryModule
 */
UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UWeaponInteractionModule : public UActorComponent
{
	GENERATED_BODY()
	GENERATED_MODULE_BODY()
	friend class AWeaponBase;
	
public: // Public functions

	UWeaponInteractionModule();
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	class AWeaponBase* GetWeapon() const
	{
		return Weapon;
	}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState() const {return MovementState;}
	UFUNCTION(BlueprintCallable)
	void SetWeapon(class AWeaponBase* Weapon_);
	
	UFUNCTION(BlueprintCallable)
	void I_Drop(const FVector& Impulse);
	void I_Drop();
	
	UFUNCTION(BlueprintCallable)
	float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) const;
	UFUNCTION(BlueprintCallable)
	void StopAnimMontage(class UAnimMontage* AnimMontage = nullptr, float BlendOut = 0.1f) const;

	int GetAvailableAmmo(int Amount);
	int RemoveAmmo(int Amount);

	template <typename WeaponWidget>
	requires TIsDerivedFrom<WeaponWidget,UUserWidget>::Value
	WeaponWidget* CreateWeaponWidget();
	void DestroyWeaponWidget();
	
	template <typename FireModeWidget>
	requires TIsDerivedFrom<FireModeWidget,UUserWidget>::Value
	FireModeWidget* CreateFireModeWidget();
	void DestroyFireModeWidget();
	
public: // Public variables
	
	UPROPERTY(BlueprintAssignable)
	FNoParam OnWeaponChange;

	UPROPERTY(EditDefaultsOnly, EditFixedSize, BlueprintReadOnly, Category = "Animation")
	TMap<TEnumAsByte<EWeaponType>, FWeaponHoldingBS> BlendSpaces;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
	USkeletalMeshComponent* SkeletalMesh;

	TDelegate<void(FVector&)> D_GetCursorLocation; 
	
protected: // Protected functions

	UFUNCTION()
	void I_PressTrigger();
	UFUNCTION()
	void I_ReleaseTrigger();
	UFUNCTION()
	void I_Reload();
	UFUNCTION()
	void I_SwitchFireMode();
	
protected: // Protected variables
	
private: // Private functions

	UFUNCTION()
	void OnMovementStateChanged();
	
private: // Private variables

	UPROPERTY()
	EMovementState MovementState = EMovementState::Static_State;
	
	UPROPERTY()
	class AWeaponBase* Weapon = nullptr;
	UPROPERTY()
	class UWeaponInventoryModule* WeaponInventoryModule = nullptr;
	UPROPERTY()
	class UAmmoInventoryModule* AmmoInventoryModule = nullptr;
	struct FAmmoSlot* CurrentAmmoSlot = nullptr;
	UPROPERTY()
	class UCharacterMovementModule* MovementModule = nullptr;
	UPROPERTY()
	UAnimInstance* AnimInstance = nullptr;
};
