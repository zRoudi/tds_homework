﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponInteraction.h"

#include "CharacterMovement.h"
#include "InputBindings.h"
#include "WeaponInventory.h"
#include "AmmoInventory.h"
#include "WidgetModule.h"

#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"


UWeaponInteractionModule::UWeaponInteractionModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]
		{
			if(const auto WidgetModule = InteractionComponent->FindModule<UWidgetModule>())
			{
				WidgetModule->AddRequirement("WeaponInteractionWidget");
			}
			WeaponInventoryModule = InteractionComponent->FindModule<UWeaponInventoryModule>();
			if(!WeaponInventoryModule)
			{
				ERROR("UWeaponInteractionModule::BeginPlay WeaponInventoryModule not found");
				return;
			}
			AmmoInventoryModule = InteractionComponent->FindModule<UAmmoInventoryModule>();
			if(!AmmoInventoryModule)
			{
				ERROR("UWeaponInteractionModule::BeginPlay AmmoInventoryModule not found");
				return;
			}
			MovementModule = InteractionComponent->FindModule<UCharacterMovementModule>();
			if(MovementModule)
			{
				MovementModule->OnMovementStateChanged.AddDynamic(this, &UWeaponInteractionModule::OnMovementStateChanged);
			}
			
			if(const auto InputBindingsModule = InteractionComponent->FindModule<UInputBindingsModule>())
			{
				InputBindingsModule->Add("Fire",this, IE_Pressed, &UWeaponInteractionModule::I_PressTrigger);
				InputBindingsModule->Add("Fire",this, IE_Released, &UWeaponInteractionModule::I_ReleaseTrigger);
				
				InputBindingsModule->Add("ReloadWeapon",this, IE_Pressed, &UWeaponInteractionModule::I_Reload);
		
				InputBindingsModule->Add("Drop",this, IE_Pressed, &UWeaponInteractionModule::I_Drop);
				
				InputBindingsModule->Add("SwitchWeaponMode",this, IE_Pressed, &UWeaponInteractionModule::I_SwitchFireMode);
			}
		}
	);
	FillMap(BlendSpaces);
	PrimaryComponentTick.bCanEverTick = false;
}

void UWeaponInteractionModule::BeginPlay()
{
	AnimInstance = (SkeletalMesh) ? SkeletalMesh->GetAnimInstance() : nullptr;
	Super::BeginPlay();
}

void UWeaponInteractionModule::SetWeapon(AWeaponBase* Weapon_)
{
	if(Weapon) Weapon->StopWeaponUse();
	Weapon = Weapon_;
	if(Weapon) Weapon->StartWeaponUse();
	OnWeaponChange.Broadcast();
}

void UWeaponInteractionModule::I_Drop(const FVector& Impulse)
{
	if(!Weapon) return;
	Weapon->StopWeaponUse();
	Weapon->I_Drop(Impulse);
	Weapon = nullptr;
	if(WeaponInventoryModule)
	{
		WeaponInventoryModule->GetCurrentSlot().SetWeapon(nullptr);
	}
	OnWeaponChange.Broadcast();
}

void UWeaponInteractionModule::I_Drop()
{
	I_Drop(GetOwner()->GetActorForwardVector() * 100);
}

float UWeaponInteractionModule::PlayAnimMontage(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName) const
{
	if (AnimInstance)
	{
		float const Duration = AnimInstance->Montage_Play(AnimMontage, InPlayRate);

		if (Duration > 0.f)
		{
			// Start at a given Section.
			if (StartSectionName != NAME_None)
			{
				AnimInstance->Montage_JumpToSection(StartSectionName, AnimMontage);
			}
			return Duration;
		}
	}
	return 0.f;
}

void UWeaponInteractionModule::StopAnimMontage(UAnimMontage* AnimMontage, float BlendOut) const
{
	if (AnimInstance)
	{
		AnimInstance->Montage_Stop(BlendOut, AnimMontage);
	}
}

int UWeaponInteractionModule::GetAvailableAmmo(int Amount)
{
	if(CurrentAmmoSlot)
		return CurrentAmmoSlot->GetAvailableAmmo(Amount);
	return 0;
}

int UWeaponInteractionModule::RemoveAmmo(int Amount)
{
	if(CurrentAmmoSlot)
		return CurrentAmmoSlot->RemoveAmmo(Amount);
	return 0;
}

template <typename WeaponWidget> requires TIsDerivedFrom<WeaponWidget, UUserWidget>::Value
WeaponWidget* UWeaponInteractionModule::CreateWeaponWidget()
{
	return nullptr;
}

void UWeaponInteractionModule::DestroyWeaponWidget()
{
}

template <typename FireModeWidget> requires TIsDerivedFrom<FireModeWidget, UUserWidget>::Value
FireModeWidget* UWeaponInteractionModule::CreateFireModeWidget()
{
	return nullptr;
}

void UWeaponInteractionModule::DestroyFireModeWidget()
{
}

void UWeaponInteractionModule::I_PressTrigger()
{
	if(Weapon)
		Weapon->I_PressTrigger();
}

void UWeaponInteractionModule::I_ReleaseTrigger()
{
	if(Weapon)
		Weapon->I_ReleaseTrigger();
}

void UWeaponInteractionModule::I_Reload()
{
	if(Weapon)
		Weapon->I_Reload();
}

void UWeaponInteractionModule::I_SwitchFireMode()
{
	if(Weapon)
		Weapon->I_SwitchFireMode();
}

void UWeaponInteractionModule::OnMovementStateChanged()
{
	MovementState = MovementModule->GetMovementState();
	if(Weapon)
	{
		Weapon->UpdateWeaponState();
	}
}

