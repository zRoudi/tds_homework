﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthSystem.h"

#include "WidgetModule.h"
#include "Components/BillboardComponent.h"
#include "Components/PanelWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHS_BarWidget-----------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

void UHS_BarWidget::OnFull()
{
}

void UHS_BarWidget::OnEmpty()
{
}

void UHS_BarWidget::OnChange()
{
	TB_State->SetText(FText::AsNumber(Bar->CurrentCapacity));
	PB_State->SetPercent((float)Bar->CurrentCapacity / (float)Bar->MaxCapacity);
}

void UHS_BarWidget::Bind(UHPBar* Bar_)
{
	Bar = Bar_;
	Bar->OnFull.AddDynamic(this,&UHS_BarWidget::OnFull);
	Bar->OnEmpty.AddDynamic(this,&UHS_BarWidget::OnEmpty);
	Bar->OnChange.AddDynamic(this, &UHS_BarWidget::OnChange);
	OnChange();
}

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHS_MainWidget----------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

void UHS_MainWidget::Init(UHealthSystemModule* Component_)
{
	Component = Component_;
	{
		const auto& Childs = BarContainer->GetAllChildren();
		for(auto& Widget : Childs)
		{
			BarContainer->RemoveChild(Widget);
		}
	}
	if(!*Component->BarWidgetClass) return;
	for(auto& Bar: Component->Bars )
	{	
		auto BarWidget = CreateWidget<UHS_BarWidget>(this,Component->BarWidgetClass);
		BarContainer->AddChild(BarWidget);
		BarWidget->Bind(Bar);
	}
}

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHPBar-------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UHPBar::UHPBar()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

void UHPBar::BeginPlay()
{
	Super::BeginPlay();
}

void UHPBar::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	RestoreCapacity(RecoilAmount);
}

int UHPBar::ApplyDamage(int Amount)
{
	if(RecoilAmount)
	{
		if(FMath::IsNearlyZero(RecoilCooldown))
		{
			PrimaryComponentTick.SetTickFunctionEnable(true);			
		}else
		{
			PrimaryComponentTick.SetTickFunctionEnable(false);
			auto&& TimerFunc = [this](){PrimaryComponentTick.SetTickFunctionEnable(true);};
			GetWorld()->GetTimerManager().SetTimer(RecoilTimerHandle,TimerFunc,RecoilCooldown,false);
		}
	}
	CurrentCapacity -= Amount;
	if(CurrentCapacity <= 0)
	{
		int LeftOver = -CurrentCapacity;
		CurrentCapacity = 0;
		OnChange.Broadcast();
		OnEmpty.Broadcast();
		return LeftOver;
	}else
	{
		OnChange.Broadcast();
		return 0;
	}
	
}

int UHPBar::RestoreCapacity(int Amount)
{
	CurrentCapacity += Amount;
	if(CurrentCapacity >= MaxCapacity)
	{
		int LeftOver = CurrentCapacity - MaxCapacity;
		CurrentCapacity = MaxCapacity;
		PrimaryComponentTick.SetTickFunctionEnable(false);
		OnChange.Broadcast();
		OnFull.Broadcast();
		return LeftOver;
	}else
	{		
		OnChange.Broadcast();
		return 0;
	}
}

void UHPBar::Init(const FHPBarParmas& Params)
{
	CurrentCapacity = Params.StartCapacity;
	MaxCapacity = Params.MaxCapacity;
	RecoilAmount = Params.RecoilAmount;
	RecoilRate = Params.RecoilRate;
	RecoilCooldown = Params.RecoilCooldown;
	HitResponse = Params.HitResponse;
	
	PrimaryComponentTick.UpdateTickIntervalAndCoolDown(RecoilRate);
}

//----------------------------------------------------------------------------------------------------------------------
//---------------------------------UHealthSystemModule---------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

UHealthSystemModule::UHealthSystemModule()
{
	UInteractionComponent::RegisterModule(
		this,
		GetOwner(),
		[this]()
		{
			if(const auto WidgetModule = InteractionComponent->FindModule<UWidgetModule>())
			{
				WidgetModule->AddRequirement("HealthSystemWidget");
			}
		}
		);
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = true;
	bEditableWhenInherited = true;
	
	BillboardComponent = CreateDefaultSubobject<UBillboardComponent>("Billboard");
	WidgetAttachmentPoint = CreateDefaultSubobject<USceneComponent>("WidgetAttachmentPoint");
	Widget = CreateDefaultSubobject<UWidgetComponent>("HealthSystemWidget");
	
	if(!GetOwner()) return;
	
	WidgetAttachmentPoint->SetupAttachment(GetOwner()->GetRootComponent());
	WidgetAttachmentPoint->SetVisibility(true);
	WidgetAttachmentPoint->SetMobility(EComponentMobility::Movable);
	WidgetAttachmentPoint->SetUsingAbsoluteRotation(true);
	WidgetAttachmentPoint->bEditableWhenInherited = true;
	
	Widget->SetupAttachment(WidgetAttachmentPoint);
	Widget->SetVisibility(true);
	Widget->SetCollisionProfileName("IgnoreAll");
	Widget->SetMobility(EComponentMobility::Movable);
	Widget->SetWidgetSpace(EWidgetSpace::World);
	Widget->SetUsingAbsoluteRotation(true);
	Widget->SetRelativeRotation(FRotator{90.0f,180.0f,0.0f});
	Widget->bEditableWhenInherited = true;
	
	BillboardComponent->SetupAttachment(Widget);
	BillboardComponent->SetMobility(EComponentMobility::Movable);
}

void UHealthSystemModule::BeginPlay()
{
	BillboardComponent->DestroyComponent();
	
	Super::BeginPlay();
	for(auto& Params : BarsParams)
	{
		if(!*Params.BarClass) continue;
		
		UHPBar* NewBar = NewObject<UHPBar>(this,Params.BarClass);
		Bars.Add(NewBar);
		GetOwner()->AddInstanceComponent(NewBar);
		NewBar->RegisterComponent();
		NewBar->Init(Params);
	}
	BarsParams.Empty();

	if(bHaseWidget)
	{
		Widget->SetWidgetClass(MainWidgetClass);
		Widget->InitWidget();
		auto MainWidget = Cast<UHS_MainWidget>(Widget->GetWidget());
		if(!MainWidget)
		{
			ERROR("UHealthSystemModule::BeginPlay Widget creation error");
			Widget->DestroyComponent();
			return;
		}
		MainWidget->Init(this);
		Widget->SetRelativeLocation(WidgetOffset);
	}else
	{
		Widget->DestroyComponent();
	}
	
	GetOwner()->OnTakeAnyDamage.AddDynamic(this,&UHealthSystemModule::OnTakeAnyDamage);
}

#if WITH_EDITORONLY_DATA
void UHealthSystemModule::OnConstruction()
{
	if(bHaseWidget)
	{
		Widget->SetRelativeLocation(WidgetOffset);		
	}
}
#else
void UHealthSystemModule::OnConstruction(){}
#endif

bool UHealthSystemModule::RestoreCapacity(int Amount)
{
	int RestoreAmount = Amount;
	for(auto& Bar:Bars)
	{
		if(!RestoreAmount) break;
		if(Bar->IsFull()) continue;
		RestoreAmount = Bar->RestoreCapacity(RestoreAmount);
	}
	return RestoreAmount!=Amount;
}

void Response(const UHS_HitResponse* HitResponse, AActor* Owner)
{
	if(HitResponse)
	{
		if(auto Size = HitResponse->Sounds.Num())
		{
			UGameplayStatics::PlaySoundAtLocation(
				Owner,
				HitResponse->Sounds[FMath::RandRange(0,Size - 1)],
				Owner->GetActorLocation()
				);
		}
		
		if(auto Size = HitResponse->DecalMaterials.Num())
		{
			FHitResult Hit;
			FCollisionQueryParams CollisionQueryParams;
			CollisionQueryParams.AddIgnoredActor(Owner);
			FVector TraceStart = Owner->GetActorLocation();
			if (Owner->GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceStart + FVector{0,0,-150}, ECC_Visibility, CollisionQueryParams))
			{
				UGameplayStatics::SpawnDecalAtLocation(
					Owner,
					HitResponse->DecalMaterials[FMath::RandRange(0,Size - 1)],
					HitResponse->DecalSize,
					Hit.Location
					);
			}
		}
		
		if(auto Size = HitResponse->Particles.Num())
		{
			UGameplayStatics::SpawnEmitterAtLocation(
				Owner,
				HitResponse->Particles[FMath::RandRange(0,Size - 1)],
				Owner->GetActorLocation()
				);
		}
	}
}

void UHealthSystemModule::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                             AController* InstigatedBy, AActor* DamageCauser)
{
	bool Damaged = false;
	int DamageToApply = Damage;
	
	for( auto& Bar:Bars)
	{
		if(!DamageToApply) break;
		if(Bar->IsEmpty()) continue;
		if(!Damaged)
		{
			Response(Bar->HitResponse,GetOwner());
			Damaged = true;
		}
		DamageToApply = Bar->ApplyDamage(DamageToApply);
	}
	
	if(DamageToApply)
	{
		OnDead.Broadcast();
		SetComponentTickEnabled(false);
		Widget->DestroyComponent();
		for(auto& Bar:Bars)
		{
			Bar->DestroyComponent();
		}
	}
}
