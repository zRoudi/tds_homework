// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTDS, Log, All);

#define ECC_Cursor ECC_GameTraceChannel1
#define ECC_Projectile ECC_GameTraceChannel2
#define ECC_Droppable ECC_GameTraceChannel3