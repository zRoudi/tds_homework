﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoSlotWidget.h"


void UAmmoSlotWidget::OnChange_Implementation()
{
}

void UAmmoSlotWidget::BindFinish_Implementation()
{
}

const FAmmoSlot UAmmoSlotWidget::GetAmmoSlot()
{
	return AmmoSlot ? *AmmoSlot : FAmmoSlot();
}

void UAmmoSlotWidget::Bind(FAmmoSlot& AmmoSlotRef)
{
	AmmoSlot = &AmmoSlotRef;
	if (!AmmoSlot) {
		ERROR("Weapon slot not valid");
		return;
	}
	AmmoSlot->OnChange.AddDynamic(this, &UAmmoSlotWidget::OnChange);
	OnChange();
	BindFinish();
}
