// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Widgets/Inventory/WeaponSlotWidget.h"

// My classes

void UWeaponSlotWidget::OnChange_Implementation()
{
}

void UWeaponSlotWidget::BindFinish_Implementation()
{
}

const FWeaponSlot UWeaponSlotWidget::GetWeaponSlot()
{
	if (WeaponSlot)
		return *WeaponSlot;
	else
		return FWeaponSlot();
}

void UWeaponSlotWidget::Bind(FWeaponSlot& WeaponSlotRef)
{
	WeaponSlot = &WeaponSlotRef;
	 if (!WeaponSlot) {
		 ERROR("Weapon slot not valid");
		 return;
	 }
	 WeaponSlot->OnChange.AddDynamic(this, &UWeaponSlotWidget::OnChange);
	 OnChange();
	 BindFinish();
}
