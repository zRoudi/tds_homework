// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Widgets/Inventory/MainInventoryWidget.h"

// My classes
#include "TDS_HomeWork/Character/TDS_HomeWorkCharacter.h"
#include "TDS_HomeWork/Functions/Macros.h"

UMainInventoryWidget::UMainInventoryWidget(const FObjectInitializer& ObjectInitializer) : Super::UUserWidget(ObjectInitializer)
{
}

void UMainInventoryWidget::NativeConstruct()
{
	// WeaponSlots setup
	{
		WeaponSlots.Empty();
		VB_WeaponSlots->ClearChildren();
		auto* WeaponInventory = GetOwningPlayerPawn<UWeaponInventoryModule>();
		if(!WeaponInventory)
		{
			ERROR("UMainInventoryWidget::NativeConstruct Owner cast to UWeaponInventoryModule feil");
			return;
		}
		if(!*WeaponSlotWidgetClass)
		{
			ERROR("UMainInventoryWidget::NativeConstruct WeaponSlotWidgetClass not bound");
			return;
		}
		auto& Slots = WeaponInventory->GetAllSlots();
		for(auto& WeaponSlot : Slots)
		{
			static int Unique = 0;
			auto NewWidget = CreateWidget<UWeaponSlotWidget>(this,WeaponSlotWidgetClass,{"WeaponSlot " + Unique++});
			WeaponSlots.Add(NewWidget);
			NewWidget->Bind(WeaponSlot);
			VB_WeaponSlots->AddChildToVerticalBox(NewWidget);
		}
	}
	// AmmoSlots setup
	{
		AmmoSlots.Empty();
		HB_AmmoSlots->ClearChildren();
		auto* AmmoInventory = UInteractionComponent::FindModule<UAmmoInventoryModule>(GetOwningPlayerPawn());
		if(!AmmoInventory)
		{
			ERROR("UMainInventoryWidget::NativeConstruct Owner cast to IAmmoInventory feil");
			return;
		}
		if(!*AmmoSlotWidgetClass)
		{
			ERROR("UMainInventoryWidget::NativeConstruct AmmoSlotWidgetClass not bound");
			return;
		}
		
		// for(auto& AmmoSlot : Slots)
		// {
		// 	static int Unique = 0;
		// 	auto NewWidget = CreateWidget<UAmmoSlotWidget>(this,AmmoSlotWidgetClass,{"AmmoSlot " + Unique++});
		// 	AmmoSlots.Add(NewWidget);
		// 	NewWidget->Bind(AmmoSlot.Value);
		// 	HB_AmmoSlots->AddChildToHorizontalBox(NewWidget);
		// }
	}
	
	Super::NativeConstruct();
}
