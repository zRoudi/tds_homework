﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

// My classes
#include "TDS_HomeWork/Interaction/Modules/AmmoInventory.h"

// Interfaces

// Generated
#include "AmmoSlotWidget.generated.h"

UCLASS()
class TDS_HOMEWORK_API UAmmoSlotWidget : public UUserWidget
{
	GENERATED_BODY()
	friend class UMainInventoryWidget;
public: // External functions

public: // External variables

protected: // Internal function
	
	UFUNCTION(BlueprintNativeEvent)
	void BindFinish();
	
	UFUNCTION(BlueprintNativeEvent)
	void OnChange();
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FAmmoSlot GetAmmoSlot();
	
	UFUNCTION(BlueprintCallable)
	void Bind(UPARAM(ref) FAmmoSlot& AmmoSlotRef);
	
protected: // Internal variables
	
	FAmmoSlot* AmmoSlot = nullptr;
};
