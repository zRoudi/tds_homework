// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

// My classes
#include "AmmoSlotWidget.h"
#include "WeaponSlotWidget.h"
#include "Components/HorizontalBox.h"
#include "Components/VerticalBox.h"
#include "TDS_HomeWork/Functions/Types.h"

// Generated
#include "MainInventoryWidget.generated.h"
/**
 *
 */
UCLASS()
class TDS_HOMEWORK_API UMainInventoryWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UMainInventoryWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;
	void Bind(class ATDS_HomeWorkCharacter* Param);

public:

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UVerticalBox* VB_WeaponSlots;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UHorizontalBox* HB_AmmoSlots;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Child Widgets",meta = (BlueprintBaseOnly = ""))
	TSubclassOf<UWeaponSlotWidget> WeaponSlotWidgetClass;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Child Widgets",meta = (BlueprintBaseOnly = ""))
	TSubclassOf<UAmmoSlotWidget> AmmoSlotWidgetClass;
	
	UPROPERTY(BlueprintReadOnly,EditFixedSize)
	TArray<UWeaponSlotWidget*> WeaponSlots;
	UPROPERTY(BlueprintReadOnly,EditFixedSize)
	TArray<UAmmoSlotWidget*> AmmoSlots;
};
