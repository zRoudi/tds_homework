// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Modules/WeaponInventory.h"

// Generated
#include "WeaponSlotWidget.generated.h"


/**
 * 
 */

UCLASS()
class TDS_HOMEWORK_API UWeaponSlotWidget : public UUserWidget
{
	GENERATED_BODY()
	friend class UMainInventoryWidget;
public:

	
protected:
	UFUNCTION(BlueprintNativeEvent)
		void BindFinish();
	UFUNCTION(BlueprintNativeEvent)
		void OnChange();
	UFUNCTION(BlueprintCallable,BlueprintPure)
		const FWeaponSlot GetWeaponSlot();
	UFUNCTION(BlueprintCallable)
		void Bind(UPARAM(ref) FWeaponSlot& WeaponSlotRef);
	FWeaponSlot* WeaponSlot = nullptr;
};