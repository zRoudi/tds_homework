// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Templates.generated.h"

UCLASS()
class TDS_HOMEWORK_API ATemplates : public AActor
{
	GENERATED_BODY()
public:

	UPROPERTY(meta = (EditCondition = "ShowOverride", EditConditionHides))
	bool temp;
	
	UPROPERTY(Category = "Default", EditAnywhere, BlueprintReadOnly)
		bool Override = false;
	UPROPERTY(Category = "Default", EditAnywhere, BlueprintReadOnly, meta = (MakeEditWidget/*FoTransform*/, EditCondition = "Override", AllowPrivateAccess = "true"))
		FTransform Param;
/* UPROPERTY()
 meta = (EditCondition = "Bool Name")
 meta = (ClampMin = "0")
 meta = (ClampMax = "0")

 



for (TFieldIterator<UProperty> PropIt(GetClass()); PropIt; ++PropIt) {
	UProperty* Property = *PropIt;
	
	some work

	}
}



	void PostEditChangeChainProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;



void Class::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *PropertyChangedEvent.GetPropertyName().ToString());
	//FMapProperty MapPropery(GetClass());

	for (TFieldIterator<FProperty> PropIt(StaticStruct()); PropIt; ++PropIt) {
		FProperty* Prop = *PropIt;
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Prop->GetName());
	}

	// Call the base class version

	super::PostEditChangeProperty(PropertyChangedEvent);
}






 */
public:	
	// Sets default values for this actor's properties
	ATemplates();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
