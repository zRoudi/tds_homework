﻿
#pragma once

/**
* @param OverrideVarName 
 * @param VarName 
 * @param Data* 
 */
#define IF_OVERRIDE_COPY(VarName)\
if(!Override##VarName)\
{\
VarName = Data->VarName;\
}

#define FIND_IN_PARAMS(VarName)\
Params.Find(#VarName, VarName)

#define ADD_TO_PARAMS(VarName)\
Params.Add(#VarName, VarName)



#define LOG(String, ...)\
UE_LOG(LogTemp,Log,TEXT(String),__VA_ARGS__)

#define WARNING(String, ...)\
UE_LOG(LogTemp,Warning,TEXT(String),__VA_ARGS__)

#define ERROR(String, ...)\
UE_LOG(LogTemp,Error,TEXT(String),__VA_ARGS__)

#define DEBUG(String,...)\
 if (GEngine)\
  GEngine->AddOnScreenDebugMessage(-1,3,FColor::Red,FString::Printf(TEXT(String),__VA_ARGS__));\
LOG(String,__VA_ARGS__);



#define GENERATE_PLACEHOLDER(ThisClass)\
operator bool() const { return this != &PlaceHolder();}\
static const ThisClass& PlaceHolder()\
 {\
  static ThisClass _{};\
  return _;\
 }



#define DATA_DIRECTORY "Data"



#define AddDynamicInterface(ObjectOwningThisInterface,InterfaceClass,Function)\
__Internal_AddDynamic(\
  ObjectOwningThisInterface,\
  MacroHelpers::Convert(ObjectOwningThisInterface, &InterfaceClass::Function),\
  #Function\
 )



namespace MacroHelpers
{
 template <typename OwnerType, typename Interface, typename RetValType, typename ...ParamTypes>
 auto Convert(OwnerType* ,RetValType (Interface::*Func)(ParamTypes...))
 {
  return reinterpret_cast<typename TBaseDynamicDelegate<FWeakObjectPtr, RetValType, ParamTypes...>
  ::template TMethodPtrResolver<OwnerType>
  ::FMethodPtr>(Func);
 }
}
