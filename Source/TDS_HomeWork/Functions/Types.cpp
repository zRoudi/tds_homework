﻿

#include "Types.h"

#include "Misc/FileHelper.h"

#include "Macros.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"
#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"
#include "TDS_HomeWork/Weapon/Bases/FireModeBase.h"
#include "TDS_HomeWork/Weapon/Projectile/DefaultProjectileLogic.h"



FWeaponDebugSettings::FWeaponDebugSettings()
{
	if (UTDS_HomeWorkGameInstance::Get()) {
		*this = UTDS_HomeWorkGameInstance::Get()->WeaponDebugSettings;
	}
	else {
		ShowDebug = false;
	}
}


bool UFileManager::SaveString(FString Directory, const FString& FileName, const FString& StringToSave,
                              bool AllowOverWriting)
{
	// set complete file path
	Directory = FPaths::ConvertRelativePathToFull(FPaths::ProjectContentDir()) + Directory;
	Directory += "\\";
	Directory += FileName;

	if(!AllowOverWriting)
	{
		if(IFileManager::Get().FileExists(*Directory))
		{
			WARNING("All Ready Exist");
			return false;
		}
	}

	return FFileHelper::SaveStringToFile(StringToSave, *Directory);
}

bool UFileManager::LoadString(FString Directory, const FString& FileName, FString& StringToLoad)
{
	// set complete file path
	Directory = FPaths::ConvertRelativePathToFull(FPaths::ProjectContentDir()) + Directory;
	Directory += "\\";
	Directory += FileName;

	if(!IFileManager::Get().FileExists(*Directory))
	{
		WARNING("UFileManager::SaveString File not exist")
		return false;
	}
	return FFileHelper::LoadFileToString(StringToLoad, *Directory);
}

void FParams::CompareAndReplace(const FParams& Params)
{
	static auto CompareKeys = []<typename A, typename B>(const TMap<A,B>& Map1, const TMap<A,B>& Map2) -> bool
	{
		TArray<A> NameArray1, NameArray2;
		Map1.GetKeys(NameArray1);
		Map2.GetKeys(NameArray2);
		return NameArray1 == NameArray2;
	};
	
	if(!CompareKeys(Float, Params.Float))
	{
		Float = Params.Float;
	}
	
	if(!CompareKeys(Int, Params.Int))
	{
		Int = Params.Int;
	}
	
	if(!CompareKeys(Bool, Params.Bool))
	{
		Bool = Params.Bool;
	}
}

void FFireModeSettings::OnChange()
{
	if(*FireModeData.FireModeClass)
	{
		FParams NewParams;
		if(const auto* FireMode = FireModeData.FireModeClass->GetDefaultObject<UFireModeBase>())
		{
			FireMode->FillParams(NewParams);
		}
		FireModeData.Params.CompareAndReplace(NewParams);
	}
	if(*ProjectileData.LogicClass)
	{
		FParams NewParams;
		if(const auto* LogicClass = ProjectileData.LogicClass->GetDefaultObject<UDefaultProjectileLogic>())
		{
			LogicClass->FillParams(NewParams);
		}
		ProjectileData.Params.CompareAndReplace(NewParams);
	}
}

void FWeaponInfo::OnDataTableChanged(const UDataTable* InDataTable, const FName InRowName)
{
	if(FirstUpdate)
	{
		//todo fix OnFirst open of file call of all updates;
		FirstUpdate = false;
		return;
	}

	for(auto& FireMode : FireModes)
	{
		FireMode.OnChange();
	}
	
	if(InDataTable)
	{		
		FString FileName = InDataTable->GetName() + ".json";
		UFileManager::SaveString(DATA_DIRECTORY,*FileName,InDataTable->GetTableAsJSON());
	}
}
