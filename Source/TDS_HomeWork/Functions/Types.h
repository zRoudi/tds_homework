// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataAsset.h"
#include "Engine/DataTable.h"
#include "Animation/BlendSpace.h"

#include "Macros.h"

#include "Types.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParam);

USTRUCT(BlueprintType)
struct FWeaponDebugSettings
{
	GENERATED_USTRUCT_BODY()
public:
	FWeaponDebugSettings();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowRequestLogs = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowWeaponDirection = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowTraceToCursor = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDispersionCone = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowExplosion = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowFireDirection = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowChangeShootDirectionLogicPoint = false;
};

UCLASS()
class TDS_HOMEWORK_API UFileManager : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable,Category = "Manage")
	static bool SaveString(FString Directory, const FString& FileName, const FString& StringToSave,
		bool AllowOverWriting = true);
	UFUNCTION(BlueprintCallable,Category = "Manage")
	static bool LoadString(FString Directory, const FString& FileName, UPARAM(Ref) FString& StringToLoad);
	
};

UENUM(BlueprintType)
enum class EWeaponType : uint8 
{
	Pistol UMETA(DisplayName = "Pistol"),
	SMG UMETA(DisplayName = "SMG"),
	Rifle UMETA(DisplayName = "Rifle"),
	Shotgun UMETA(DisplayName = "Shotgun"),
	Sniper UMETA(DisplayName = "Sniper"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher")
};

UENUM(BlueprintType)
enum class EMiscType : uint8 
{
	Grenade UMETA(DisplayName = "Grenade"),
	SmallHealthPack UMETA(DisplayName = "SmallHealthPack"),
	LargeHealthPack UMETA(DisplayName = "LargeHealthPack"),
	SmallShieldPack UMETA(DisplayName = "SmallShieldPack"),
	LargeShieldPack UMETA(DisplayName = "LargeShieldPack"),
};

UENUM(BlueprintType)
enum class EMovementState : uint8 
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Static_State UMETA(DisplayName = "Static State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

UENUM(BlueprintType)
enum class EHPType: uint8
{
	Life UMETA(DisplayName = "Life"),
	Armor UMETA(DisplayName = "Armor"),
	Shield UMETA(DisplayName = "Shield")
};


template<typename T>
void FillMap(TMap<TEnumAsByte<EMovementState>, T>& Map) {
	Map.Add(EMovementState::Aim_State);
	Map.Add(EMovementState::Static_State);
	Map.Add(EMovementState::Walk_State);
	Map.Add(EMovementState::Sprint_State);
}

inline const TCHAR* EnumToString(EMovementState InCurrentState)
{
	switch (InCurrentState)
	{
	case EMovementState::Aim_State:
		return TEXT("Aim State");
	case EMovementState::Static_State:
		return TEXT("Static State");
	case EMovementState::Walk_State:
		return TEXT("Walk State");
	case EMovementState::Sprint_State:
		return TEXT("Sprint State");
	default:
		WARNING("Add Movement State");
		return TEXT("Unknown");	
	}
}

template<typename T>
void FillMap(TMap<TEnumAsByte<EWeaponType>, T>& Map) {
	Map.Add(EWeaponType::Pistol);
	Map.Add(EWeaponType::SMG);
	Map.Add(EWeaponType::Rifle);
	Map.Add(EWeaponType::Shotgun);
	Map.Add(EWeaponType::Sniper);
	Map.Add(EWeaponType::RocketLauncher);
}

inline const TCHAR* EnumToString(EWeaponType InCurrentState)
{
	switch (InCurrentState)
	{
	case EWeaponType::Pistol:
		return TEXT("Pistol");
	case EWeaponType::SMG:
		return TEXT("SMG");
	case EWeaponType::Rifle:
		return TEXT("Rifle");
	case EWeaponType::Shotgun:
		return TEXT("Shotgun");
	case EWeaponType::Sniper:
		return TEXT("Sniper");
	case EWeaponType::RocketLauncher:
		return TEXT("RocketLauncher");
	default:
		WARNING("Add Weapon Type");
		return TEXT("Unknown");
	}
}

template<typename T>
void FillMap(TMap<TEnumAsByte<EPhysicalSurface>, T>& Map) {
	Map.Add(EPhysicalSurface::SurfaceType_Default);
	Map.Add(EPhysicalSurface::SurfaceType1);
	Map.Add(EPhysicalSurface::SurfaceType2);
}

inline const TCHAR* EnumToString(EPhysicalSurface InCurrentState)
{
	switch (InCurrentState)
	{
	case SurfaceType_Default:
		return TEXT("SurfaceType_Default");
	case SurfaceType1:
		return TEXT("SurfaceType1");
	case SurfaceType2:
		return TEXT("SurfaceType2");
	default:
		WARNING("Add Surface Type");
		return TEXT("Unknown");
	}	
}

USTRUCT(BlueprintType)
struct FWidgetIcons
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* Weapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* Ammo = nullptr;
};

USTRUCT(BlueprintType)
struct FAnimationData
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimMontage")
		UAnimMontage* Anim = nullptr;
	UPROPERTY(BlueprintReadOnly, Category = "Settings")
		float PlayRate = 1.0f;

	FORCEINLINE void SetPlayRate(const float& AnimDuration){
		if (Anim)
			PlayRate = Anim->SequenceLength / AnimDuration;
	}
};

USTRUCT(BlueprintType)
struct FPressedButtons
{
	GENERATED_USTRUCT_BODY()
public:
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buttons")
	bool Sprint = false;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buttons")
	bool Aim = false;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buttons")
	bool Fire = false;

};
 
USTRUCT(BlueprintType)
struct FDispersionParams
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Max")
		float Max = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Min")
		float Min = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Recoil")
		float Recoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reduction")
		float Reduction = 0.3f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		FDispersionParams Roll;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		FDispersionParams Pitch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Roll")
		FDispersionParams Yaw;
};

USTRUCT(BlueprintType)
struct FPhysicalSurfaceImpact
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UMaterialInterface* Decal = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		USoundBase* Sound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* Particles = nullptr;
};

USTRUCT(BlueprintType)
struct FCameraAim
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Range")
		float MinRange = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Range")
		float MaxRange = 500.0f;
};

USTRUCT(BlueprintType)
struct FWeaponHoldingBS
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Range")
		UBlendSpace* BS_Hip = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Range")
		UBlendSpace* BS_Aim = nullptr;
};

USTRUCT(BlueprintType)
struct FParams
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, EditFixedSize)
	TMap<FName, float> Float;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, EditFixedSize)
	TMap<FName, int> Int;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, EditFixedSize)
	TMap<FName, bool> Bool;
	
	void CompareAndReplace(const FParams &Params);
	
	FORCEINLINE void Add(const FName& Key, const bool& Value) {
		Bool.Add(Key, Value);
	}

	FORCEINLINE void Add(const FName& Key, const float& Value) {
		Float.Add(Key, Value);
	}

	FORCEINLINE void Add(const FName& Key, const int& Value) {
		Int.Add(Key, Value);
	}

	FORCEINLINE bool Find(const FName& Key, bool& ReturnValue) {
		if (const auto* Result = Bool.Find(Key))
		{
			ReturnValue = *Result;
			return true;
		}
		return false;
	}

	FORCEINLINE bool Find(const FName& Key, float& ReturnValue) {
		if (const auto* Result = Float.Find(Key))
		{
			ReturnValue = *Result;
			return true;
		}
		return false;
	}

	FORCEINLINE bool Find(const FName& Key, int& ReturnValue) {
		if (const auto* Result = Int.Find(Key))
		{
			ReturnValue = *Result;
			return true;
		}
		return false;
	}
};

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UHitData : public UDataAsset
{
	GENERATED_BODY()
public:
	UHitData(){
		FillMap(Impact);
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float Damage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Impact")
		TMap<TEnumAsByte<EPhysicalSurface>, FPhysicalSurfaceImpact> Impact;
};

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UExplosionData : public UDataAsset
{
	GENERATED_BODY()
public:
	UExplosionData(){
		FillMap(Impact);
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float MaxDamage = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float MinDamage = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Sphere")
		float OuterRadius = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Sphere")
		float InnerRadius = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Sphere")
		float Falloff = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Impact")
		TMap<TEnumAsByte<EPhysicalSurface>, FPhysicalSurfaceImpact> Impact;
}; 

UCLASS(BlueprintType)
class TDS_HOMEWORK_API UTraceData : public UDataAsset
{
	GENERATED_BODY()
public:
	UTraceData(){
		FillMap(Impact);
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float DistanceTrace = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		float Damage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Impact")
		TMap<TEnumAsByte<EPhysicalSurface>, FPhysicalSurfaceImpact> Impact;
};


USTRUCT(BlueprintType)
struct FDispersionData
{
	GENERATED_BODY()
public:
	FDispersionData(){
		FillMap(States);
	}
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<TEnumAsByte<EMovementState>, FWeaponDispersion> States;
};

USTRUCT(BlueprintType)
struct FProjectileData
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class UDefaultProjectileLogic> LogicClass;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FParams Params;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* Effect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* Mesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* Sound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTraceData* TraceData = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UHitData* HitData = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UExplosionData* ExplosionData = nullptr;
};

USTRUCT(BlueprintType)
struct FReloadData
{
	GENERATED_BODY()
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool OnlyManualReload = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool CanStopReload = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool ReloadFullMagazine = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", EditCondition = "!ReloadFullMagazine"))
	int32 PartialReloadSize = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* Sound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnimationData CharacterAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnimationData WeaponAnim;
};
 
USTRUCT(BlueprintType)
struct FFireFXData
{
	GENERATED_BODY()
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* Sound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* Effect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnimationData CharacterHipAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnimationData CharacterAimAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FAnimationData WeaponAnim;
};

USTRUCT(BlueprintType)
struct FFireModeData
{
	GENERATED_BODY()
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UFireModeBase> FireModeClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FParams Params;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon = nullptr;
};


USTRUCT(BlueprintType)
struct FFireModeSettings
{
	GENERATED_BODY()
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FFireModeData FireModeData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FProjectileData ProjectileData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FFireFXData FireFXData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FDispersionData DispersionData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReloadData ReloadData;
	
	void OnChange();
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()
	bool FirstUpdate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon",meta = (BlueprintBaseOnly = ""))
	TSubclassOf<class AWeaponBase> WeaponBase;

	UPROPERTY()
	bool HaseAimMode = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aim", meta = (EditCondition = "HaseAimMode"))
	float AimDistance = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireModeSettings")
	TArray<FFireModeSettings> FireModes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireModeSettings", meta = (ClampMin = "0"))
	float ModeSwitchCooldown_NotImplemented = 1.0f; //todo
	
	virtual void OnDataTableChanged(const UDataTable* InDataTable, FName InRowName) override;
};


USTRUCT(BlueprintType)
struct FWeaponTypeTable
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	UDataTable* WeaponDT = nullptr;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FWidgetIcons Icons;	
};