// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "TDS_HomeWork/Functions/Types.h"

#include "TDS_HomeWorkGameInstance.generated.h"

/**
 * 
 */

struct FAmmoPickupParams;
UCLASS()
class TDS_HOMEWORK_API UTDS_HomeWorkGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	virtual void OnStart() override;
	virtual void Shutdown() override;
	UTDS_HomeWorkGameInstance();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Icons")
	TMap<TEnumAsByte<EWeaponType>, FWeaponTypeTable> WeaponTables;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
	FWeaponDebugSettings WeaponDebugSettings;
	
	FORCEINLINE static const UTDS_HomeWorkGameInstance* Get()
	{
		return GI;
	}
	
protected:
	friend class AWeaponBase;
	inline static UTDS_HomeWorkGameInstance* GI = nullptr;
	FWeaponInfo* GetWeaponInfo(EWeaponType Type, FName& Name) const;
};
