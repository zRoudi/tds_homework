// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_HomeWorkPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "TDS_HomeWork/Character/TDS_HomeWorkCharacter.h"
#include "Engine/World.h"

ATDS_HomeWorkPlayerController::ATDS_HomeWorkPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
	bEnableMouseOverEvents = true;
	bEnableClickEvents = true;
}

void ATDS_HomeWorkPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ATDS_HomeWorkPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// // support touch devices 
	// InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATDS_HomeWorkPlayerController::MoveToTouchLocation);
	// InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ATDS_HomeWorkPlayerController::MoveToTouchLocation);
	//
	// InputComponent->BindAction("ResetVR", IE_Pressed, this, &ATDS_HomeWorkPlayerController::OnResetVR);
}
