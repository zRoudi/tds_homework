// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWorkPlayerController.generated.h"

UCLASS()
class ATDS_HomeWorkPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDS_HomeWorkPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

};


