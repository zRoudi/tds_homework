// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"

#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Enviroment/Pickups/AmmoPickup.h"


void UTDS_HomeWorkGameInstance::OnStart()
{
	GI = this;
}

void UTDS_HomeWorkGameInstance::Shutdown()
{
	Super::Shutdown();
	GI = nullptr;
}

UTDS_HomeWorkGameInstance::UTDS_HomeWorkGameInstance()
{
	FillMap(WeaponTables);
}

FWeaponInfo* UTDS_HomeWorkGameInstance::GetWeaponInfo(EWeaponType Type, FName& Name) const
{
	const FWeaponTypeTable* WeaponTypeTable = WeaponTables.Find(Type);
	if(!WeaponTypeTable)
	{
		ERROR("UTDS_HomeWorkGameInstance::GetWeaponInfo - WeaponTables not conteining type: %s", EnumToString(Type));
		return nullptr;
	}
	
	UDataTable* WeaponTable = WeaponTypeTable->WeaponDT;
	if (!WeaponTable)
	{
		ERROR("UTPSGameInstance::GetWeaponInfo - WeaponTable not bound");
		return nullptr;
	}
	return WeaponTable->FindRow<FWeaponInfo>(Name, "", false);
}