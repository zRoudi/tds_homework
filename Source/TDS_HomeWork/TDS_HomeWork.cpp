// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_HomeWork.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_HomeWork, "TDS_HomeWork" );

DEFINE_LOG_CATEGORY(LogTDS)
 