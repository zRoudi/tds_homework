// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

// Core classes
#include "CoreMinimal.h"
#include "GameFramework/Character.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"

// Modules
#include "TDS_HomeWork/Interaction/Interaction.h"

// Generated
#include "TDS_HomeWorkCharacter.generated.h"


UCLASS(Blueprintable)
class ATDS_HomeWorkCharacter :	public ACharacter, 
								public IInteractionInterface
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public: // Components
	
	//Modules
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	UInteractionComponent* InteractionComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UInputBindingsModule* InputBindingsModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UHealthSystemModule* HealthSystemModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UWeaponInventoryModule* WeaponInventoryModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UAmmoInventoryModule* AmmoInventoryModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UCharacterCameraModule* CharacterCameraModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UCharacterMovementModule* CharacterMovementModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UPickupInteractionModule* PickupInteractionModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UWeaponInteractionModule* WeaponInteractionModule;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Modules")
	class UWidgetModule* WidgetModule;
	
	
	// Widget
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Widget")
	class UMainInventoryWidget* MainInventoryWidget = nullptr;

public:	//External functions
	
	// Constructor
	ATDS_HomeWorkCharacter();
	
	// Tick
	virtual void Tick(float DeltaSeconds) override;

	// Get's
	UFUNCTION(BlueprintCallable)
	bool HoldsWeapon();


	//Input
	virtual void SetupPlayerInputComponent(class UInputComponent* Param) override;
	
	UFUNCTION()
	void InputOpenCloseInventory();

	UFUNCTION()
	void OnDead();
	
public: // External variables;

	UPROPERTY(EditAnywhere,BlueprintAssignable,Category = "Weapon")
	FNoParam OnWeaponChange;
	
	UPROPERTY(BlueprintReadWrite, Category = "Buttons")
	FPressedButtons States;
	
	float CameraCurrentOffset = 0.0f;
	
protected: // Internal functions	

	// Misc
	virtual void RerunConstructionScripts() override;

protected: // Internal variables
	
	// Pickup
	class IPickupInterface* LastPickup = nullptr;
	FTimerHandle PickupTimerHandle;

private:
	friend class IWeaponInventory;
};