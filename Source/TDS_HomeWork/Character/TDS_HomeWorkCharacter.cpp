// Copyright Epic Games, Inc. All Rights Reserved.


#include "TDS_HomeWork/Character/TDS_HomeWorkCharacter.h"

// Core classes
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"

// My classes
#include "TDS_HomeWork/Functions/Macros.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"
#include "TDS_HomeWork/Interaction/Modules/CharacterCamera.h"
#include "TDS_HomeWork/Interaction/Modules/CharacterMovement.h"
#include "TDS_HomeWork/Interaction/Modules/HealthSystem.h"
#include "TDS_HomeWork/Interaction/Modules/InputBindings.h"
#include "TDS_HomeWork/Interaction/Modules/Pickup.h"
#include "TDS_HomeWork/Interaction/Modules/WeaponInteraction.h"
#include "TDS_HomeWork/Interaction/Modules/WeaponInventory.h"
#include "TDS_HomeWork/Interaction/Modules/WidgetModule.h"

#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"
#include "TDS_HomeWork/Widgets/Inventory/MainInventoryWidget.h"


void ATDS_HomeWorkCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Constructor
ATDS_HomeWorkCharacter::ATDS_HomeWorkCharacter()
{
	// Create a Interaction component
	InteractionComponent = CreateDefaultSubobject<UInteractionComponent>(TEXT("InteractionComponent"));
	
	// Create a InputBindings Module
	InputBindingsModule = CreateDefaultSubobject<UInputBindingsModule>(TEXT("InputBindingsModule"));

	// Create a AmmoInventory Module
	AmmoInventoryModule = CreateDefaultSubobject<UAmmoInventoryModule>(TEXT("AmmoInventoryModule"));
	
	// Create a HealthSystem Module
	HealthSystemModule = CreateDefaultSubobject<UHealthSystemModule>(TEXT("HealthSystemModule"));
	HealthSystemModule->OnDead.AddDynamic(this,&ATDS_HomeWorkCharacter::OnDead);

	// Create a CharacterCamera Module
	CharacterCameraModule = CreateDefaultSubobject<UCharacterCameraModule>(TEXT("CharacterCameraModule"));
	CharacterCameraModule->SetupAttachment(GetRootComponent());
	
	// Create a CharacterMovement Module
	CharacterMovementModule = CreateDefaultSubobject<UCharacterMovementModule>(TEXT("CharacterMovementModule"));
	
	// Create a PickupInteraction Module
	PickupInteractionModule = CreateDefaultSubobject<UPickupInteractionModule>(TEXT("PickupInteractionModule"));
	PickupInteractionModule->D_GetOverlappedActor.BindLambda([this](AActor*& Actor)
	{
		Actor = CharacterMovementModule->LastCursorHitResult.GetActor();
	});
	
	// Create a WeaponInventory Module
	WeaponInventoryModule = CreateDefaultSubobject<UWeaponInventoryModule>(TEXT("WeaponInventoryModule"));
	WeaponInventoryModule->WeaponAttachmentComponent = GetMesh();
	WeaponInventoryModule->SocketName = TEXT("WeaponSocketRightHand");

	// Create a WeaponInteraction Module
	WeaponInteractionModule = CreateDefaultSubobject<UWeaponInteractionModule>(TEXT("WeaponInteractionModule"));
	WeaponInteractionModule->SkeletalMesh = GetMesh();
	WeaponInteractionModule->D_GetCursorLocation.BindLambda([this](FVector& Location)
	{
		Location = CharacterMovementModule->LastCursorHitResult.Location;
	});
	
	// Create a Widget Module
	WidgetModule = CreateDefaultSubobject<UWidgetModule>(TEXT("WidgetModule"));
	
	// Finish creation of modules
	InteractionComponent->FinishModuleRegistration();
	
	// Set player capsule params
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	GetCapsuleComponent()->SetCollisionProfileName("Pawn");
	GetCapsuleComponent()->SetGenerateOverlapEvents(true);

	// Set Skeletal mesh params
	GetMesh()->SetCollisionProfileName("PawnBody");
	GetMesh()->SetGenerateOverlapEvents(true);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

// Tick
void ATDS_HomeWorkCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

// Get's 
bool ATDS_HomeWorkCharacter::HoldsWeapon()
{
	return (bool)WeaponInteractionModule->GetWeapon();
}

// inputs
void ATDS_HomeWorkCharacter::SetupPlayerInputComponent(UInputComponent* Param)
{
	Super::SetupPlayerInputComponent(Param);
	InputComponent->BindAction(TEXT("OpenCloseInventory"),IE_Pressed,this,&ATDS_HomeWorkCharacter::InputOpenCloseInventory);
}

void ATDS_HomeWorkCharacter::InputOpenCloseInventory()
{
	if(InputBindingsModule->bIsPaused)
	{
		InputBindingsModule->UnpauseGame();
	}else
	{
		InputBindingsModule->PauseGame();
	}
	if(!(MainInventoryWidget)) return;
	if(MainInventoryWidget->Visibility == ESlateVisibility::Visible)
	{
		MainInventoryWidget->SetVisibility(ESlateVisibility::Hidden);
	}else
	{
		MainInventoryWidget->SetVisibility(ESlateVisibility::Visible);		
	}
}

void ATDS_HomeWorkCharacter::OnDead()
{
	DisableInput(nullptr);
	GetMesh()->SetCollisionProfileName("Ragdoll",false);
	GetMesh()->SetSimulatePhysics(true);
	GetCharacterMovement()->DisableMovement();
}

void ATDS_HomeWorkCharacter::RerunConstructionScripts()
{
	Super::RerunConstructionScripts();
	HealthSystemModule->OnConstruction();
}






