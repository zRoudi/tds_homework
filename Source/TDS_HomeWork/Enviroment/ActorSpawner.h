// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorSpawner.generated.h"

UCLASS()
class TDS_HOMEWORK_API AActorSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActorSpawner();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		bool SpawnersEnabled = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		bool DoubleSided = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		bool OnFireEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		float OnFireChance = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		bool IsOpenEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		float IsOpenChance = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		int MinHieght = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		int MaxHieght = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		int Lenght = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		float IndentX = 270.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		float IndentY = -615.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		float IndentZ = 270.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default Settings")
		float RandomOffset = 5.f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable)
		void ConstructionInitializer();


	UFUNCTION(BlueprintImplementableEvent)
		void EachComponents(FTransform Transform, bool IsSpawner, bool IsOpen, bool OnFire);

private:
	void ConstructionIterations(float Indent = 0, float angle = 0);
};
