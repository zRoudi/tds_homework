﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HPPickup.h"

#include "TDS_HomeWork/Interaction/Modules/HealthSystem.h"
#include "TDS_HomeWork/Interaction/Modules/Pickup.h"


void AHPPickup::ExecuteLogic(AActor* OtherActor)
{
	const auto HealthSystemModule = UInteractionComponent::FindModule<UHealthSystemModule>(OtherActor);
	if(!HealthSystemModule) return;
	if(HealthSystemModule->RestoreCapacity(RestoreAmount))
	{	
		PickupModule->Deactivate();
		Destroy();
	}
}

AHPPickup* AHPPickup::SpawnHPPickup(UObject* WorldContextObject, int HPAmount, const FVector& Location,
	const FRotator& Rotation, const FVector& Impulse)
{
	if(!WorldContextObject)
	{
		ERROR("AAmmoPickup::SpawnAmmoPickup WorldContextObject not valid");
		return nullptr;
	}
	UWorld* World = WorldContextObject->GetWorld();
	if(!World)
	{
		ERROR("AAmmoPickup::SpawnAmmoPickup WorldContextObject GetWorld feil");
		return nullptr;
	}

	auto Pickup = World->SpawnActor<AHPPickup>(StaticClass(),Location,Rotation);
	if(!Pickup)
	{
		ERROR("AAmmoPickup::SpawnAmmoPickup Spawning error");
		return nullptr;
	}
	Pickup->RestoreAmount = HPAmount;
	Pickup->GetStaticMeshComponent()->AddImpulse(Impulse);
	return Pickup;
}
