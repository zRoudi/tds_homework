﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupBase.h"

#include "TDS_HomeWork/TDS_HomeWork.h"
#include "TDS_HomeWork/Interaction/Modules/Pickup.h"

#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"

APickupBase::APickupBase()
{
	PrimaryActorTick.bCanEverTick = false;

	InteractionComponent = CreateDefaultSubobject<UInteractionComponent>(TEXT("InteractionComponent"));
	
	PickupModule = CreateDefaultSubobject<UPickupModule>("Pickup");
	PickupModule->PickupBodyMesh = GetStaticMeshComponent();
	PickupModule->bGenerateOverlap = true;
	PickupModule->bGenerateCursorOver = true;

	PickupModule->D_OnClicked.BindLambda([this](AActor* ClickInstigator)
	{
		if((ClickInstigator->GetActorLocation() - GetActorLocation()).Size() > PickupModule->Range ) return;
		ExecuteLogic(ClickInstigator);
	});
	
	PickupModule->D_OnClickedContinuous.BindLambda([this](AActor* ClickInstigator)
	{
		if((ClickInstigator->GetActorLocation() - GetActorLocation()).Size() > PickupModule->Range ) return;
		
		TArray<FOverlapResult> ResultArray;
	
		FCollisionObjectQueryParams Params;
		Params.AddObjectTypesToQuery(ECC_Droppable);
	
		FCollisionShape Shape;
		Shape.SetSphere(PickupModule->Range);
		if(!GetWorld()->OverlapMultiByObjectType(ResultArray,ClickInstigator->GetActorLocation(),
			{},Params,Shape)) return;
		
		for (auto& Result : ResultArray)
		{
			if(const auto AmmoPickup = Cast<APickupBase>(Result.Actor))
			{
				AmmoPickup->ExecuteLogic(ClickInstigator);
			}
		}
	});
	
	PickupModule->OnOverlap.BindUObject(this, &APickupBase::ExecuteLogic);
	
	InteractionComponent->FinishModuleRegistration();
}

// Called when the game starts or when spawned
void APickupBase::BeginPlay()
{
	Super::BeginPlay();
	PickupModule->Activate();
}