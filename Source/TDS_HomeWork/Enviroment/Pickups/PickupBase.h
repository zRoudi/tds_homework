﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"

#include "Components/WidgetComponent.h"
#include "Engine/StaticMeshActor.h" 
#include "TDS_HomeWork/Interaction/Interaction.h"

#include "PickupBase.generated.h"




UCLASS(BlueprintType,Abstract)
class TDS_HOMEWORK_API APickupBase
	: public AStaticMeshActor
	, public IInteractionInterface
{
	GENERATED_BODY()
	
public:
	
	APickupBase();
	
	virtual void BeginPlay() override;
	
	virtual void ExecuteLogic(AActor* OtherActor){}
	
protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Modules")
	UInteractionComponent* InteractionComponent;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Modules")
	class UPickupModule* PickupModule;
};