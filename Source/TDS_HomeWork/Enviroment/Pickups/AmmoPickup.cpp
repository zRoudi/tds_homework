﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"

#include "TDS_HomeWork/Functions/Macros.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"
#include "TDS_HomeWork/Interaction/Modules/AmmoInventory.h"
#include "TDS_HomeWork/Interaction/Modules/Pickup.h"


void AAmmoPickup::ExecuteLogic(AActor* ClickInstigator)
{
	if(const auto AmmoModule = UInteractionComponent::FindModule<UAmmoInventoryModule>(ClickInstigator))
	{
		if(AmmoModule->IsMax(AmmoType)) return;
		AmmoModule->AddAmmo(AmmoType, AmmoAmount);
		AmmoAmount = 0;
		PickupModule->Deactivate();
		Destroy();
	}	
}

AAmmoPickup* AAmmoPickup::SpawnAmmoPickup(UObject* WorldContextObject, EWeaponType Type,
		const FVector& Location, const FRotator& Rotation,const FVector& Impulse)
{
	if(!WorldContextObject)
	{
		ERROR("AAmmoPickup::SpawnAmmoPickup WorldContextObject not valid");
		return nullptr;
	}
	UWorld* World = WorldContextObject->GetWorld();
	if(!World)
	{
		ERROR("AAmmoPickup::SpawnAmmoPickup WorldContextObject GetWorld feil");
		return nullptr;
	}

	auto Pickup = World->SpawnActor<AAmmoPickup>(StaticClass(),Location,Rotation);
	if(!Pickup)
	{
		ERROR("AAmmoPickup::SpawnAmmoPickup Spawning error");
		return nullptr;
	}

	Pickup->AmmoType = Type;
	Pickup->GetStaticMeshComponent()->AddImpulse(Impulse);
	return Pickup;
}

