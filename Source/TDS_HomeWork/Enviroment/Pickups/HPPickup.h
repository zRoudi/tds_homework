﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "PickupBase.h"
#include "TDS_HomeWork/Functions/Types.h"

// Interfaces

// Generated
#include "HPPickup.generated.h"

UCLASS(Blueprintable,BlueprintType)
class TDS_HOMEWORK_API AHPPickup : public APickupBase
{
	GENERATED_BODY()

public: 

	UPROPERTY(EditAnywhere,Category = "HP")
	int RestoreAmount = 10;

	virtual void ExecuteLogic(AActor* OtherActor) override;


	UFUNCTION(BlueprintCallable, meta = (HideSelfPin))
	static AHPPickup* SpawnHPPickup(UObject* WorldContextObject, int HPAmount,
		const FVector& Location = FVector::ZeroVector, const FRotator& Rotation = FRotator::ZeroRotator,
		const FVector& Impulse = FVector::ZeroVector);
};
