﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupBase.h"
#include "TDS_HomeWork/Functions/Types.h"

#include "AmmoPickup.generated.h"


UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API AAmmoPickup : public APickupBase
{
	GENERATED_BODY()
	
protected:	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup")
	int AmmoAmount = 10;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup")
	EWeaponType AmmoType = EWeaponType::Pistol;

private:

	virtual void ExecuteLogic(AActor* OtherActor) override;

public:
	
	UFUNCTION(BlueprintCallable, meta = (HideSelfPin))
	static AAmmoPickup* SpawnAmmoPickup(UObject* WorldContextObject, EWeaponType Type,
		const FVector& Location = FVector::ZeroVector, const FRotator& Rotation = FRotator::ZeroRotator,
		const FVector& Impulse = FVector::ZeroVector);
};