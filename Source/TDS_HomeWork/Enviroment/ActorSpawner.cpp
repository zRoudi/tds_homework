// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Enviroment/ActorSpawner.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AActorSpawner::AActorSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
}

// Called when the game starts or when spawned
void AActorSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AActorSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AActorSpawner::ConstructionInitializer()
{
	ConstructionIterations();
	if (DoubleSided)
		ConstructionIterations(IndentY, 180);
}


void AActorSpawner::ConstructionIterations(float Indent, float angle)
{
	bool IsSpawner;
	FVector SpawnLocation;
	FVector Offset;
	bool onFire;
	bool isOpen;
	int SpawnerNum = -1;
	for (int i = 0; i < Lenght; i++) {
		int Hieght = UKismetMathLibrary::RandomIntegerInRange(MinHieght, MaxHieght);
		if (SpawnersEnabled) {
			SpawnerNum = UKismetMathLibrary::RandomIntegerInRange(1, Hieght - 1);
		}
		SpawnLocation = FVector(i * IndentX, Indent, 0);
		for (int y = 0; y < Hieght; y++) {
			IsSpawner = false;
			if (y == SpawnerNum)
				IsSpawner = true;
			Offset = FVector(UKismetMathLibrary::RandomFloatInRange(-RandomOffset, RandomOffset), UKismetMathLibrary::RandomFloatInRange(-RandomOffset, RandomOffset), 0);
			onFire = (OnFireEnabled) && (UKismetMathLibrary::RandomFloatInRange(0, 1) <= OnFireChance) ? true : false;
			isOpen = (IsOpenEnabled) && (UKismetMathLibrary::RandomFloatInRange(0, 1) <= IsOpenChance) ? true : false;
			EachComponents(FTransform(FRotator(0, angle + UKismetMathLibrary::RandomFloatInRange(-0.5f,0.5f), 0), SpawnLocation + Offset), IsSpawner, isOpen, onFire);
			SpawnLocation.Z += IndentZ;
		}
	}
}

