// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TDS_HomeWork/Weapon/Projectile/DefaultProjectileLogic.h"

#include "BounceProjectileLogic.generated.h"

/**
 * 
 */
UCLASS()
class TDS_HOMEWORK_API UBounceProjectileLogic : public UDefaultProjectileLogic
{
	GENERATED_BODY()
public:
	UBounceProjectileLogic();
	virtual void FillParams(FParams& Params) const override;
	virtual bool SetPramsFromInfo() override;
	virtual bool Init() override;
	virtual void BindEvents() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void OnEndPlay() override;

	virtual void OnHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

protected:
	float GravityScale = 0.0f;

};
