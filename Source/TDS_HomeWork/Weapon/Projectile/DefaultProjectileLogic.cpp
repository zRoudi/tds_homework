// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Weapon/Projectile/DefaultProjectileLogic.h"

#include "TDS_HomeWork/Weapon/Bases/ProjectileBase.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"

#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "TDS_HomeWork/Functions/Macros.h"

void UDefaultProjectileLogic::FillParams(FParams& Params) const
{
	ADD_TO_PARAMS(LifeTime);
	ADD_TO_PARAMS(Speed);
}

// Sets default values for this component's properties
UDefaultProjectileLogic::UDefaultProjectileLogic()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UDefaultProjectileLogic::BeginPlay()
{
	Super::BeginPlay();
	
	Projectile->SetLifeSpan(LifeTime);
	
}

bool UDefaultProjectileLogic::Init()
{
	Projectile = Cast<AProjectileBase>(GetOwner());
	if(!Projectile || !SetPramsFromInfo())
		return false;
	

	Projectile->BulletProjectileMovement->ProjectileGravityScale = 0;
	Projectile->BulletProjectileMovement->Velocity = FVector(Speed, 0.0f, 0.0f);

	BindEvents();
	Projectile->FinishAndRegisterComponent(this);
	return true;
}

void UDefaultProjectileLogic::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	auto& Data = Projectile->ProjectileData;
	FWeaponDebugSettings debug;

	if (OtherActor)
	{
		bool isPhysMaterialValid = Hit.PhysMaterial.IsValid();

		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		auto HitData = Data.HitData;
		if (HitData) {

			if (isPhysMaterialValid && HitData->Impact.Contains(mySurfacetype))
			{
				FPhysicalSurfaceImpact HitImpactData = HitData->Impact[mySurfacetype];
				if (HitImpactData.Decal && OtherComp)
					UGameplayStatics::SpawnDecalAttached(HitImpactData.Decal, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
				if (HitImpactData.Particles)
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitImpactData.Particles, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
				if (HitImpactData.Sound)
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitImpactData.Sound, Hit.ImpactPoint);
			}
			UGameplayStatics::ApplyDamage(
				OtherActor,
				HitData->Damage,
				Projectile->GetInstigatorController(),
				Projectile,
				NULL
			);
		}

		auto ExplosionData = Data.ExplosionData;
		if (ExplosionData) {
			if (isPhysMaterialValid && ExplosionData->Impact.Contains(mySurfacetype))
			{
				FPhysicalSurfaceImpact ExplosionImpactData = ExplosionData->Impact[mySurfacetype];
				if (ExplosionImpactData.Decal && OtherComp)
					UGameplayStatics::SpawnDecalAttached(ExplosionImpactData.Decal, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
				if (ExplosionImpactData.Particles)
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionImpactData.Particles, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
				if (ExplosionImpactData.Sound)
					UGameplayStatics::PlaySoundAtLocation(this, ExplosionImpactData.Sound, Hit.ImpactPoint);
			}
			TArray<AActor*> IgnoredActors;
			UGameplayStatics::ApplyRadialDamageWithFalloff(
				this,
				ExplosionData->MaxDamage,
				ExplosionData->MinDamage,
				Hit.ImpactPoint,
				ExplosionData->InnerRadius,
				ExplosionData->OuterRadius,
				ExplosionData->Falloff,
				NULL,
				IgnoredActors,
				Projectile,
				Projectile->GetInstigatorController()
			);

			if (debug.ShowDebug && debug.ShowExplosion) {
				DrawDebugSphere(GetWorld(), Hit.ImpactPoint, ExplosionData->OuterRadius, 16, FColor::Red, false, 1.0f);
				DrawDebugSphere(GetWorld(), Hit.ImpactPoint, ExplosionData->InnerRadius, 16, FColor::Blue, false, 1.0f);
			}
		}
	}
	if (debug.ShowDebug)
		DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 10.f, 8, FColor::Blue, false, 4.0f);

	Projectile->Destroy();
}

bool UDefaultProjectileLogic::SetPramsFromInfo()
{
	if(!Projectile) return false;
	auto& Params = Projectile->ProjectileData.Params;
	
	bool Result = true;
	
	Result &= FIND_IN_PARAMS(LifeTime);
	Result &= FIND_IN_PARAMS(Speed);

	return Result;
}

void UDefaultProjectileLogic::BindEvents()
{
	Projectile->BulletCollisionSphere->OnComponentHit.AddDynamic(this, &UDefaultProjectileLogic::OnHit);
}
