// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Weapon/Projectile/BounceProjectileLogic.h"

#include "TDS_HomeWork/Weapon/Bases/ProjectileBase.h"

#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "TDS_HomeWork/Functions/Macros.h"

UBounceProjectileLogic::UBounceProjectileLogic()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UBounceProjectileLogic::FillParams(FParams& Params) const
{
	Super::FillParams(Params);
	
	ADD_TO_PARAMS(GravityScale);
}

bool UBounceProjectileLogic::SetPramsFromInfo()
{
	if(!Super::SetPramsFromInfo()) return false;

	auto& Params = Projectile->ProjectileData.Params;
	
	bool Result = true;
	
	Result &= FIND_IN_PARAMS(GravityScale);

	return Result;
}

bool UBounceProjectileLogic::Init()
{
	if(!Super::Init()) return false;

	Projectile->BulletProjectileMovement->ProjectileGravityScale = GravityScale;
	Projectile->BulletProjectileMovement->bShouldBounce = true;
	
	return true;;
}

void UBounceProjectileLogic::BindEvents()
{
	Projectile->BulletCollisionSphere->OnComponentHit.AddDynamic(this, &UBounceProjectileLogic::OnHit);
}

void UBounceProjectileLogic::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
}

void UBounceProjectileLogic::OnEndPlay()
{

	auto& Data = Projectile->ProjectileData;

	FWeaponDebugSettings debug;
	
	if (const auto ExplosionData = Data.ExplosionData) {
		if (ExplosionData->Impact.Contains(EPhysicalSurface::SurfaceType_Default))
		{
			auto& [Decal, Sound, Particles] = ExplosionData->Impact[EPhysicalSurface::SurfaceType_Default];
			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				Particles,
				Projectile->GetActorTransform()
				);
			UGameplayStatics::PlaySoundAtLocation(
				this,
				Sound,
				Projectile->GetActorLocation()
				);
		}
		UGameplayStatics::ApplyRadialDamageWithFalloff(
			this,
			ExplosionData->MaxDamage,
			ExplosionData->MinDamage,
			Projectile->GetActorLocation(),
			ExplosionData->InnerRadius,
			ExplosionData->OuterRadius,
			ExplosionData->Falloff,
			nullptr,
			{},
			Projectile,
			Projectile->GetInstigatorController()
		);
		if (debug.ShowDebug && debug.ShowExplosion) {
			DrawDebugSphere(
				GetWorld(),
				Projectile->GetActorLocation(),
				ExplosionData->OuterRadius,
				16,
				FColor::Red,
				false,
				1.0f
				);
			DrawDebugSphere(
				GetWorld(),
				Projectile->GetActorLocation(),
				ExplosionData->InnerRadius,
				16,
				FColor::Blue,
				false,
				1.0f
				);
		}
	}
}

void UBounceProjectileLogic::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	FWeaponDebugSettings debug;
	if (debug.ShowDebug)
		DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 10.f, 8, FColor::Blue, false, 4.0f);
}