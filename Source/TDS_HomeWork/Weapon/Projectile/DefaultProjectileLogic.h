// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "TDS_HomeWork/Functions/Types.h"

#include "DefaultProjectileLogic.generated.h"

class AProjectileBase;

UCLASS(ClassGroup = (Custom), Blueprintable)
class TDS_HOMEWORK_API UDefaultProjectileLogic : public UActorComponent
{
	GENERATED_BODY()
public:	
	virtual void FillParams(FParams& Params) const;
	UDefaultProjectileLogic();
	virtual void BeginPlay() override;
	virtual bool Init();
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override {};
	virtual void OnEndPlay(){};
	UFUNCTION()
	virtual void OnHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){};
	UFUNCTION()
	virtual void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {};
protected:
	virtual bool SetPramsFromInfo();
	// Refs
	virtual void BindEvents();
	AProjectileBase* Projectile;
	
	// Params
	float LifeTime = 20.0f;
	float Speed = 2000.0f;
};
