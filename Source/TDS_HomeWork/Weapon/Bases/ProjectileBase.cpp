// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Weapon/Bases/ProjectileBase.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

#include "TDS_HomeWork/Weapon/Projectile/DefaultProjectileLogic.h"

#include "Kismet/GameplayStatics.h"

#include "DrawDebugHelpers.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(2.f);
	BulletCollisionSphere->SetCollisionProfileName(FName("Projectile"));
	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial
	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)
	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetCanEverAffectNavigation(false);
	BulletMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	BulletMesh->SetupAttachment(RootComponent);

	
	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->bAutoActivate = false;
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->ProjectileGravityScale = 0.0f;
	BulletProjectileMovement->bShouldBounce = false;
	BulletProjectileMovement->SetUpdatedComponent(RootComponent);
}

void AProjectileBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (EndPlayReason != EEndPlayReason::Type::Destroyed) return;
	Logic->OnEndPlay();
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	if(!Initialised)
	{
		ERROR("AProjectileBase::BeginPlay not initialized");
		Destroy();
		return;
	}
	Super::BeginPlay();
	if (ProjectileData.Effect) {
		BulletFX->Template = ProjectileData.Effect;
		BulletFX->Activate();
	}
	else {
		BulletFX->DestroyComponent();
	}

	if (ProjectileData.Mesh) {
		BulletMesh->SetStaticMesh(ProjectileData.Mesh);
	}
	else {
		BulletMesh->DestroyComponent();
	}

	if (ProjectileData.Sound) {
		BulletSound->Sound = ProjectileData.Sound;
		BulletSound->Activate();
	}
	else {
		BulletSound->DestroyComponent();
	}
	
	
	BulletProjectileMovement->Activate();
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AProjectileBase::Init(const FProjectileData& InitParam)
{
	ProjectileData = InitParam;
	
	if (*ProjectileData.LogicClass) {
		Logic = NewObject<UDefaultProjectileLogic>(this, ProjectileData.LogicClass, TEXT("Logic class"));
		
		if (!(Logic && Logic->Init())) {
			ERROR("AProjectileBase::Init - ProjectileLogic creation error");
			return false;
		}
	}
	else {
		ERROR("AProjectileBase::Init - LogicClass not valid");
		return false;
	}
	Initialised = true;
	return true;
}

void AProjectileBase::OnHit(const FProjectileData& Data, const FHitResult& Hit, AActor* DamageCauser)
{
	if (!DamageCauser) return;
	AActor* OtherActor = Hit.GetActor();
	if(!OtherActor) return;
	
	UPrimitiveComponent* OtherComp = Hit.GetComponent();
	const FWeaponDebugSettings Debug;

	const bool bIsPhysMaterialValid = Hit.PhysMaterial.IsValid();

	const EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

	if (const auto HitData = Data.HitData) {
		if (bIsPhysMaterialValid && HitData->Impact.Contains(MySurfaceType))
		{
			const auto [Decal, Sound, Particles] = HitData->Impact[MySurfaceType];
			if (Decal && OtherComp)
				UGameplayStatics::SpawnDecalAttached(
					Decal,
					FVector{20.0f},
					OtherComp,
					NAME_None,
					Hit.ImpactPoint,
					Hit.ImpactNormal.Rotation(),
					EAttachLocation::KeepWorldPosition,
					10.0f
					);
			if (Particles)
				UGameplayStatics::SpawnEmitterAtLocation(
					DamageCauser->GetWorld(),
					Particles,
					{
						Hit.ImpactNormal.Rotation(),
						Hit.ImpactPoint,
						FVector{1.0f}
						}
					);
			if (Sound)
				UGameplayStatics::PlaySoundAtLocation(
					DamageCauser->GetWorld(),
					Sound,
					Hit.ImpactPoint
					);
		}
		
		UGameplayStatics::ApplyDamage(
			OtherActor,
			HitData->Damage,
			DamageCauser->GetInstigatorController(),
			DamageCauser,
			nullptr
		);
	}
	
	if (const auto ExplosionData = Data.ExplosionData)
	{
		if (bIsPhysMaterialValid && ExplosionData->Impact.Contains(MySurfaceType))
		{
			const auto [Decal, Sound, Particles] = ExplosionData->Impact[MySurfaceType];
			UGameplayStatics::SpawnDecalAttached(
				Decal,
				FVector{20.0f},
				OtherComp,
				NAME_None,
				Hit.ImpactPoint,
				Hit.ImpactNormal.Rotation(),
				EAttachLocation::KeepWorldPosition,
				10.0f
				);
			UGameplayStatics::SpawnEmitterAtLocation(
				DamageCauser->GetWorld(),
				Particles,
				{
					Hit.ImpactNormal.Rotation(),
					Hit.ImpactPoint,
					FVector{1.0f}
					}
				);
			UGameplayStatics::PlaySoundAtLocation(
				DamageCauser->GetWorld(),
				Sound,
				Hit.ImpactPoint
				);
		}
		
		UGameplayStatics::ApplyRadialDamageWithFalloff(
			DamageCauser->GetWorld(),
			ExplosionData->MaxDamage,
			ExplosionData->MinDamage,
			Hit.ImpactPoint,
			ExplosionData->InnerRadius,
			ExplosionData->OuterRadius,
			ExplosionData->Falloff,
			nullptr,
			{},
			DamageCauser,
			DamageCauser->GetInstigatorController()
		);

		if (Debug.ShowDebug && Debug.ShowExplosion) {
			DrawDebugSphere(
				DamageCauser->GetWorld(),
				Hit.ImpactPoint,
				ExplosionData->OuterRadius,
				16,
				FColor::Red,
				false,
				1.0f
				);
			DrawDebugSphere(
				DamageCauser->GetWorld(),
				Hit.ImpactPoint,
				ExplosionData->InnerRadius,
				16,
				FColor::Blue,
				false,
				1.0f
				);
		}
	}
	
	if (Debug.ShowDebug)
	{
		DrawDebugSphere(
			DamageCauser->GetWorld(),
			 Hit.ImpactPoint,
			 10.f,
			 8,
			 FColor::Blue,
			 false,
			 4.0f
			 );
	}
}