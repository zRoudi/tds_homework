// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"

// Core classes
#include "Kismet/GameplayStatics.h"

// My classes
#include "GameFramework/Character.h"
#include "TDS_HomeWork/TDS_HomeWork.h"
#include "TDS_HomeWork/Weapon/Bases/FireModeBase.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"
#include "TDS_HomeWork/Weapon/Bases/ProjectileBase.h"

#include "TDS_HomeWork/Interaction/Modules/WeaponInteraction.h"
#include "TDS_HomeWork/Interaction/Modules/Pickup.h"
#include "TDS_HomeWork/Interaction/Modules/WeaponInventory.h"

AWeaponBase::AWeaponBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	
	// Create a 
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>("Skeletal Mesh");
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	// Create a 
	ShootLocation = CreateDefaultSubobject<UArrowComponent>("Shoot Location");
	ShootLocation->SetupAttachment(RootComponent);
	
	// Create a 
	ShellDropLocation = CreateDefaultSubobject<UArrowComponent>("Shell Drop Location");
	ShellDropLocation->SetupAttachment(RootComponent);

	// Create a 
	MagazineDropLocation = CreateDefaultSubobject<UArrowComponent>("Magazine Drop Location");
	MagazineDropLocation->SetupAttachment(RootComponent);

	// Create a 
	InteractionComponent = CreateDefaultSubobject<UInteractionComponent>("InteractionComponent");
	
	// Create a 
	PickupModule = CreateDefaultSubobject<UPickupModule>("PickupModule");
	PickupModule->bGenerateCursorOver = true;
	PickupModule->bGenerateOverlap = false;
	PickupModule->PickupBodyMesh = SkeletalMeshWeapon;
	PickupModule->D_OnClicked.BindLambda([this](AActor* ClickInstigator)
	{
		DEBUG("AWeaponBase::OnClicked_Lambda");
		const auto WI = UInteractionComponent::FindModule<UWeaponInventoryModule>(ClickInstigator);
		if(!WI) 
		{
			ERROR("Click instigator not owning weapon inventory module")
			return;
		}
		if(!WI->AddWeapon(this))
		{
			WARNING("Cannot add weapon")
			return;
		}
		PickupModule->Deactivate();
		SkeletalMeshWeapon->DetachFromComponent(
			{
				EDetachmentRule::KeepWorld,
				false
			}
			);
		SkeletalMeshWeapon->AttachToComponent(
			RootComponent,
			{
				EAttachmentRule::KeepWorld,
				true
			}
			);
		SkeletalMeshWeapon->SetRelativeTransform(
			FTransform{DefaultMeshRotation},
			false
			);
	});
	PickupModule->D_OnClickedContinuous.BindLambda([this](AActor* ClickInstigator)
	{
		DEBUG("AWeaponBase::OnClickedContinuous_Lambda");
		const auto WI = UInteractionComponent::FindModule<UWeaponInventoryModule>(ClickInstigator);
		if(!WI)
		{
			ERROR("Click instigator not owning weapon inventory module")
			return;
		}
		WI->ReplaceWeapon(this);
		PickupModule->Deactivate();
		SkeletalMeshWeapon->DetachFromComponent(
			{
				EDetachmentRule::KeepWorld,
				false
			}
			);
		SkeletalMeshWeapon->AttachToComponent(
			RootComponent,
			{
				EAttachmentRule::KeepWorld,
				true
			}
			);
		SkeletalMeshWeapon->SetRelativeTransform(
			FTransform{DefaultMeshRotation},
			false
			);
	});

	// Finish creation of modules 
	InteractionComponent->FinishModuleRegistration();
}

// AActor Override
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWeaponBase::AttachTo(USceneComponent* Parent, const FName& Socket)
{
	DEBUG("AWeaponBase::AttachTo Parent:%s Socket: %s", *Parent->GetName(), *Socket.ToString());
	if(!Parent)
	{
		ERROR("AWeaponBase::AttachTo Parent not provided");
		return;
	}
	SetOwner(Parent->GetOwner());
	SetInstigator(GetOwner<APawn>());
	WeaponInteractionModule = UInteractionComponent::FindModule<UWeaponInteractionModule>(GetOwner());
	if(!WeaponInteractionModule)
	{
		ERROR("AWeaponBase::AttachTo ParentActor not owning WeaponInteractionModule");
		return;
	}
	for(const auto & FireMode : FireModes)
	{
		FireMode->WeaponInteractionModule = WeaponInteractionModule;
		FireMode->OnAttached();
	}
	AttachToComponent(Parent,{EAttachmentRule::SnapToTarget, true},Socket);
}

void AWeaponBase::I_Drop(const FVector& Impulse)
{
	if(InUse)
	{
		ERROR("AWeaponBase::I_Drop Cannnot drop weapon, in use");
		return;
	}
	for(const auto & FireMode : FireModes)
	{
		FireMode->WeaponInteractionModule = nullptr;
		FireMode->OnDropped();
	}
	SetHidden(false);
	PickupModule->Activate();
	SkeletalMeshWeapon->AddImpulse(Impulse);
	WeaponInteractionModule = nullptr;
}

bool AWeaponBase::StartWeaponUse()
{
	if (!Initialized) {
		ERROR("AWeaponBase::StartWeaponUse - not initialised ");
		return false;
	}
	if (InUse)
	{
		WARNING("AWeaponBase::StartWeaponUse allready in use");
		return false;
	}
	if(!WeaponInteractionModule)
	{
		WARNING("AWeaponBase::StartWeaponUse Weapon not attached");
		return false;
	}	
	
	FireModes[CurrentFireMode]->StartUse();
	
	SetActorTickEnabled(true);
	SetActorHiddenInGame(false);
	OnStartWeaponUse.Broadcast();
	InUse = true;
	return true;
}

void AWeaponBase::StopWeaponUse()
{
	if (!InUse)
	{
		WARNING("AWeaponBase::StopWeaponUse allready not in use");
		return;
	}
	
	SetActorTickEnabled(false);
	SetActorHiddenInGame(true);

	
	TriggerPressed = false;
	
	for(const auto & FireMode : FireModes)
	{
		FireMode->StopUse();
	}
	
	StopAnimMontage();
	OnStopWeaponUse.Broadcast();
	InUse = false;
}


void AWeaponBase::I_PressTrigger()
{
	if (!TriggerPressed) {
		TriggerPressed = true;
		FireModes[CurrentFireMode]->OnTriggerPressed();
	}
}

void AWeaponBase::I_ReleaseTrigger()
{
	if (TriggerPressed) {
		TriggerPressed = false;
		FireModes[CurrentFireMode]->OnTriggerReleased();
	}
}


bool AWeaponBase::I_SwitchFireMode()
{
	if (FireModes.Num() > 1)
	{
		FireModes[CurrentFireMode]->StopUse();
		CurrentFireMode = (CurrentFireMode + 1) % FireModes.Num();
		FireModes[CurrentFireMode]->StopUse();

		OnSwitchFireMode.Broadcast();
		return true;
	}else
	{
		const auto Debug = UTDS_HomeWorkGameInstance::Get()->WeaponDebugSettings;
		if(Debug.ShowDebug && Debug.ShowRequestLogs)
			LOG("AWeaponBase::I_SwitchFireMode - Cannot switch");
		return false;
	}
}

void AWeaponBase::I_Reload()
{
	FireModes[CurrentFireMode]->I_Reload();
}


void AWeaponBase::UpdateWeaponState()
{
	FireModes[CurrentFireMode]->OnMovementStateChanged();
}

const FWeaponInfo& AWeaponBase::GetInfo() const
{
	return WeaponInfo;
}

// Init Internal
bool AWeaponBase::Init()
{	
	if (!SkeletalMeshWeapon->SkeletalMesh)
	{
		ERROR("AWeaponBase::Init Weapon SkeletalMesh not bound");
		return false;
	}
	if(WeaponInfo.FireModes.Num() == 0)
	{
		ERROR("AWeaponBase::Init WeaponInfo has no FireModes");
		return false;
	}
	for(auto & FireModeData: WeaponInfo.FireModes)
	{
		static int Unique = 0;
		auto* FireMode = NewObject<UFireModeBase>(this, *FireModeData.FireModeData.FireModeClass, *FString::Printf(TEXT("FireMode_%d"),++Unique));
		if (FireMode && FireMode->Init(FireModeData)) {
			AddInstanceComponent(FireMode);
			FireModes.Add(FireMode);
		}
		else {
			ERROR("AWeaponBase::Init - FireMode Initialisation Error");
			return false;
		}
	}	
	return FinishInit();
}

bool AWeaponBase::FinishInit()
{
	Initialized = true;
	return true;
}

// AActor Override
void AWeaponBase::BeginPlay()
{
	if(!Initialized)
	{
		ERROR("AWeaponBase::BeginPlay not initialised");
		Destroy();
		return;
	}
	AnimInstance = (SkeletalMeshWeapon) ? SkeletalMeshWeapon->GetAnimInstance() : nullptr;
	Super::BeginPlay();
}

// AnimMontage
float AWeaponBase::PlayAnimMontage(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName) const
{
	if (AnimInstance)
	{
		float const Duration = AnimInstance->Montage_Play(AnimMontage, InPlayRate);

		if (Duration > 0.f)
		{
			// Start at a given Section.
			if (StartSectionName != NAME_None)
			{
				AnimInstance->Montage_JumpToSection(StartSectionName, AnimMontage);
			}
			return Duration;
		}
	}
	return 0.f;
}

void AWeaponBase::StopAnimMontage(UAnimMontage* AnimMontage, float BlendOut) const
{
	if (AnimInstance)
	{
		AnimInstance->Montage_Stop(BlendOut, AnimMontage);
	}
}

// WeaponSpawner
AWeaponBase* AWeaponBase::SpawnWeapon(UObject* WorldContextObject, EWeaponType Type, FName& Name, const FVector& Location,
	const FRotator& Rotation)
{	
	if(!WorldContextObject)
	{
		WARNING("AWeaponBase::SpawnWeapon WorldContextObject not valid");
		return nullptr;
	}
	UWorld* World = WorldContextObject->GetWorld();
	if(!World)
	{
		WARNING("AWeaponBase::SpawnWeapon WorldContextObject GetWorld feil");
		return nullptr;
	}
	auto GI = UTDS_HomeWorkGameInstance::Get();
	if(!GI)
	{
		ERROR("AWeaponBase::SpawnWeapon GI not valid");
		return nullptr;
	}
	
	FWeaponInfo* WeaponInfo = GI->GetWeaponInfo(Type,Name);
	if (!WeaponInfo) {
		ERROR("AWeaponBase::SpawnWeapon - WeaponInfo by name %s not found", *Name.ToString());
		return nullptr;
	}
	
	if (!WeaponInfo || !WeaponInfo->WeaponBase)
	{
		ERROR("AWeaponBase::SpawnWeapon - Info not valid");
		return nullptr;
	}
	
	auto* Weapon = World->SpawnActorDeferred<AWeaponBase>(
		WeaponInfo->WeaponBase,
		{Rotation,Location},
		nullptr,
		nullptr,
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn
		);
	if (!Weapon)
	{
		ERROR("AWeaponBase::SpawnWeapon - Spawning Error");
		return nullptr;
	}
	Weapon->WeaponType = Type;
	Weapon->WeaponName = Name;
	Weapon->WeaponInfo = *WeaponInfo;

	if (!Weapon->Init())
	{
		ERROR("AWeaponBase::SpawnWeapon - Initialisation Error");
		Weapon->Destroy();
		return nullptr;
	}
	Weapon->FinishSpawning(FTransform(), true);
	return Weapon;
}

AWeaponBase* AWeaponBase::SpawnWeaponAsPickup(UObject* WorldContextObject, EWeaponType Type, FName& Name,
		const FVector& Location, const FRotator& Rotation, const FVector& Impulse)
{
	const auto Weapon = SpawnWeapon(WorldContextObject,Type,Name,Location,Rotation);
	if(!Weapon)
	{
		ERROR("AWeaponBase::SpawnWeaponAsPickup Spawning error");
		return nullptr;
	}
	Weapon->PickupModule->Activate();
	Weapon->SkeletalMeshWeapon->AddImpulse(Impulse);
	return Weapon;
}

AProjectileBase* AWeaponBase::FireProjectile(const UObject* WorldContextObject, const FVector& Location, const FRotator& Rotation, const FActorSpawnParameters& Params, const FProjectileData& Data)
{
	auto Projectile = WorldContextObject->GetWorld()->SpawnActorDeferred<AProjectileBase>(
				AProjectileBase::StaticClass(),
				{},
				Params.Owner,
				Params.Instigator,
				Params.SpawnCollisionHandlingOverride
				);
	
	if (!Projectile)
	{
		ERROR("UFireModeBase::FireProjectile - Projectile creation error");
		return nullptr;
	}
	
	if(!Projectile->Init(Data))
	{
		Projectile->Destroy();
		ERROR("UFireModeBase::FireProjectile - Projectile initialisation error");
		return nullptr;
	}
	
	Projectile->FinishSpawning({Rotation,Location});
	return Projectile;
}

FHitResult AWeaponBase::FireTrace(const UObject* WorldContextObject, const FVector& Begin, const FVector& End, const FCollisionQueryParams& CollisionQueryParams)
{
	FHitResult HitResult;
	WorldContextObject->GetWorld()->LineTraceSingleByChannel(HitResult, Begin, End, ECC_Projectile, CollisionQueryParams);
	return HitResult;
}