// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HomeWork/Weapon/Bases/FireModeBase.h"

// Core classes

// My classes
#include "TDS_HomeWork/Functions/Macros.h"
#include "TDS_HomeWork/Weapon/Bases/ProjectileBase.h"
#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"

UFireModeBase::UFireModeBase()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	UActorComponent::SetAutoActivate(true);
}

bool UFireModeBase::Init(FFireModeSettings& InitFireMode)
{
	Weapon = GetOwner<AWeaponBase>();
	if(!Weapon) {
		ERROR("UFireModeBase::Init - Weapon not valid");
		DestroyComponent();
		return false;
	}
	
	WeaponInfo		= &Weapon->WeaponInfo;
	FireModeData	= &InitFireMode.FireModeData;
	ProjectileData	= &InitFireMode.ProjectileData;
	FireFXData		= &InitFireMode.FireFXData;
	DispersionData	= &InitFireMode.DispersionData;
	ReloadData		= &InitFireMode.ReloadData;
	
	if (!SetPramsFromInfo()) {
		ERROR("UFireModeBase::Init - SetPramsFromInfo Error");
		DestroyComponent();
		return false;
	}
	
	return OnInit();
}