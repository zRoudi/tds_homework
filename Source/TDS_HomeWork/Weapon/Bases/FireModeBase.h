// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"

// Interfaces

// Generated
#include "FireModeBase.generated.h"

/// <summary>
/// Default Auto Mode
/// </summary>

class AWeaponBase;
struct FWeaponInterface;

UCLASS(ClassGroup = (Custom), Blueprintable, Abstract)
class TDS_HOMEWORK_API UFireModeBase : public UActorComponent
{
	GENERATED_BODY()
	friend class UFireNotify;
	friend class AWeaponBase;

public: // Public functions

	virtual void FillParams(UPARAM(ref) FParams& Params) const {}
	virtual bool SetPramsFromInfo() {return true;}

	UFireModeBase();
	
	virtual void OnAttached(){}
	virtual void OnDropped(){}
	
	virtual void StartUse(){}
	virtual void StopUse(){}
	
	virtual void OnTriggerPressed(){}
	virtual void OnTriggerReleased(){}

	virtual void OnMovementStateChanged(){}

	virtual void I_Reload(){}
	
public: // Public variables

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "FireModeData")
	FName FireModeName = NAME_None;
	
protected: // Protected functions

	// Init related
	bool Init(FFireModeSettings& InitFireMode);
	virtual bool OnInit(){return true;}

	virtual void Fire(FVector SpawnLocation){}
	
protected: // Protected variables
	
	// Links
	UPROPERTY()
	class UWeaponInteractionModule* WeaponInteractionModule = nullptr;
	UPROPERTY()
	AWeaponBase* Weapon = nullptr;
	
	FWeaponInfo* WeaponInfo = nullptr;
	FFireModeData* FireModeData = nullptr;
	FProjectileData* ProjectileData = nullptr;
	FFireFXData* FireFXData = nullptr;
	FDispersionData* DispersionData = nullptr;
	FReloadData* ReloadData = nullptr;

public:
	

	
};