// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// Core classes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

// My classes
#include "ProjectileBase.h"
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Interaction/Interaction.h"

// Modules

// Generated
#include "WeaponBase.generated.h"

UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API AWeaponBase
	: public AActor
	, public IInteractionInterface
{
	GENERATED_BODY()
private:

	friend class UWeaponInteractionModule;
	friend class UFireNotify;
	friend class UFireModeBase;
	
	bool Initialized = false;
public:	//Components
	
	AWeaponBase();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UArrowComponent* ShellDropLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UArrowComponent* MagazineDropLocation = nullptr;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Modules")
	UInteractionComponent* InteractionComponent;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Modules")
	class UPickupModule* PickupModule;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FireMode")
	TArray<UFireModeBase*> FireModes;


public:	// External functions
	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void AttachTo(USceneComponent* Parent, const FName& Socket);
	UFUNCTION()
	void I_Drop(const FVector& Impulse);
	
	UFUNCTION(BlueprintCallable)
	bool StartWeaponUse();
	UFUNCTION(BlueprintCallable)
	void StopWeaponUse();

	UFUNCTION(BlueprintCallable)
	void I_PressTrigger();
	UFUNCTION(BlueprintCallable)
	void I_ReleaseTrigger();

	UFUNCTION(BlueprintCallable)
	bool I_SwitchFireMode();
	UFUNCTION(BlueprintCallable)
	void I_Reload();

	UFUNCTION(BlueprintCallable)
	void UpdateWeaponState();
	UFUNCTION(BlueprintCallable)
	const FWeaponInfo& GetInfo() const;
	
	// AnimMontage
	float PlayAnimMontage(UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) const;
	void StopAnimMontage(UAnimMontage* AnimMontage = nullptr, float BlendOut = 0.1f) const;
	bool IsTriggerPressed() const
	{
		return TriggerPressed;
	}

public: // External Variables
	
	float SizeVectorToChangeShootDirectionLogic = 300.0f;
	
	UPROPERTY(BlueprintAssignable)
	FNoParam OnStartWeaponUse;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnStopWeaponUse;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnSwitchFireMode;

protected: //Internal functions
	// Init Internal
	bool Init();	
	bool FinishInit();
	
	
	// AActor Override
	virtual void BeginPlay() override;


protected: //Internal variables
	// Flags
	bool Attached = false;
	bool InUse = false;
	bool TriggerPressed = false;
	
	// FireModes
	UPROPERTY()
	int CurrentFireMode = 0;

	//Params

	// Refs
	UPROPERTY()
	class UWeaponInteractionModule* WeaponInteractionModule = nullptr;
	UPROPERTY()
	UAnimInstance* AnimInstance;
	
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WeaponInfo")
	FName WeaponName = NAME_None;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WeaponInfo")
	EWeaponType WeaponType = EWeaponType::Pistol;
	
	FWeaponInfo WeaponInfo;
	// SaveState
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FRotator DefaultMeshRotation;

	
public: // WeaponSpawner
	UFUNCTION(BlueprintCallable, meta = (HideSelfPin))
	static AWeaponBase* SpawnWeapon(UObject* WorldContextObject, EWeaponType Type, UPARAM(Ref)FName& Name,
		const FVector& Location = FVector::ZeroVector, const FRotator& Rotation = FRotator::ZeroRotator);
	
	// todo randomise direction
	UFUNCTION(BlueprintCallable, meta = (HideSelfPin))
	static AWeaponBase* SpawnWeaponAsPickup(UObject* WorldContextObject, EWeaponType Type, UPARAM(Ref)FName& Name,
		const FVector& Location = FVector::ZeroVector, const FRotator& Rotation = FRotator::ZeroRotator,
		const FVector& Impulse = FVector::ZeroVector);
	
	static class AProjectileBase* FireProjectile(const UObject* WorldContextObject, const FVector& Location,
		const FRotator& Rotation, const FActorSpawnParameters& Params,	const FProjectileData& Data);
	static FHitResult FireTrace(const UObject* WorldContextObject, const FVector& Begin, const FVector& End,
								const FCollisionQueryParams& CollisionQueryParams);
};