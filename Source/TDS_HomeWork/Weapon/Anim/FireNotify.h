﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "FireNotify.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TDS_HOMEWORK_API UFireNotify : public UAnimNotify
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FName Socket;
	
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	virtual void BranchingPointNotify(FBranchingPointNotifyPayload& BranchingPointPayload) override;
	
	UFireNotify();
	virtual ~UFireNotify() override {};
protected:
	class AWeaponBase* Owner = nullptr;
	FTransform SpawnTransform;
};
