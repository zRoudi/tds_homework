﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "DropNotify.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TDS_HOMEWORK_API UDropNotify : public UAnimNotify
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	UStaticMesh* Mesh = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FName Socket;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float Velocity;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FRotator Direction;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float LifeSpan;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float Mass;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float Dispersion;
	
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	virtual void BranchingPointNotify(FBranchingPointNotifyPayload& BranchingPointPayload) override;
	
	void Drop();

	
	UDropNotify();
	virtual ~UDropNotify() override {};
protected:
	UWorld* World = nullptr;
	FTransform SpawnTransform;
};
