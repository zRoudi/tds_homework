﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "DropNotify.h"

#include "Engine/StaticMeshActor.h"
#include "TDS_HomeWork/Functions/Macros.h"
#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"


void UDropNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	UAnimNotify::Notify(MeshComp, Animation);
	
	World = FNavigationSystem::GetWorldFromContextObject(MeshComp);
	
	SpawnTransform = MeshComp->GetSocketTransform(Socket);

	Drop();
	
}

void UDropNotify::BranchingPointNotify(FBranchingPointNotifyPayload& BranchingPointPayload)
{
	UAnimNotify::BranchingPointNotify(BranchingPointPayload);
	World = FNavigationSystem::GetWorldFromContextObject(BranchingPointPayload.SkelMeshComponent);

	Drop();
}


void UDropNotify::Drop()
{
	if(!(World && Mesh)) return;		
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	auto DroppedMesh = World->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), SpawnTransform, SpawnParams);

	if (DroppedMesh && DroppedMesh->GetStaticMeshComponent()){
		DroppedMesh->SetActorTickEnabled(false);
		DroppedMesh->SetLifeSpan(LifeSpan);
		
		auto StaticMesh = DroppedMesh->GetStaticMeshComponent();
		StaticMesh->Mobility = EComponentMobility::Movable;
		StaticMesh->SetStaticMesh(Mesh);
		StaticMesh->SetCollisionProfileName(TEXT("Droppable"));
		StaticMesh->SetSimulatePhysics(true);
		StaticMesh->SetMassOverrideInKg(NAME_None, Mass);
			
		if(!FMath::IsNearlyZero(Velocity)){
			auto Impulse = (SpawnTransform.Rotator() + Direction).Vector();
			if (!FMath::IsNearlyZero(Dispersion))
				{
					Impulse = FMath::VRandCone(Impulse, Dispersion / 180 * PI);
				}
			Impulse *= Velocity;
			DroppedMesh->GetStaticMeshComponent()->AddImpulse(Impulse);
		}
	}
}

UDropNotify::UDropNotify()
{
}
