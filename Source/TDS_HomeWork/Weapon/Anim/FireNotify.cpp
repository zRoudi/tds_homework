﻿// Fill out your copyright notice in the Description page of Project Settings.




#include "FireNotify.h"

#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"
#include "TDS_HomeWork/Weapon/Bases/FireModeBase.h"


void UFireNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	UAnimNotify::Notify(MeshComp, Animation);
	if(!MeshComp) return;
	
	Owner = MeshComp->GetOwner<AWeaponBase>();
	if(Owner)
	{
		Owner->FireModes[Owner->CurrentFireMode]->Fire(
			MeshComp->GetSocketLocation(Socket)
			);
	}
}

void UFireNotify::BranchingPointNotify(FBranchingPointNotifyPayload& BranchingPointPayload)
{
	UAnimNotify::BranchingPointNotify(BranchingPointPayload);	
}

UFireNotify::UFireNotify()
{
}
