﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AutoFireMode.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "TDS_HomeWork/Game/TDS_HomeWorkGameInstance.h"
#include "TDS_HomeWork/Interaction/Modules/HealthSystem.h"
#include "TDS_HomeWork/Interaction/Modules/WeaponInteraction.h"
#include "TDS_HomeWork/Weapon/Bases/ProjectileBase.h"
#include "TDS_HomeWork/Weapon/Bases/WeaponBase.h"
#include "TDS_HomeWork/Weapon/Projectile/DefaultProjectileLogic.h"


void UAutoFireMode::FillParams(FParams& Params) const
{
	ADD_TO_PARAMS(bApplyDispersion);
	ADD_TO_PARAMS(FireRate);
	ADD_TO_PARAMS(MagazineSize);
	ADD_TO_PARAMS(ProjectilePerShot);
	ADD_TO_PARAMS(AmmoUsagePerShot);
}

bool UAutoFireMode::SetPramsFromInfo()
{
	auto& Params = FireModeData->Params;

	bool Result = true;
	
	Result &= FIND_IN_PARAMS(bApplyDispersion);
	Result &= FIND_IN_PARAMS(FireRate);
	Result &= FIND_IN_PARAMS(MagazineSize);
	Result &= FIND_IN_PARAMS(ProjectilePerShot);
	Result &= FIND_IN_PARAMS(AmmoUsagePerShot);
		
	return Result;
}

// Sets default values
UAutoFireMode::UAutoFireMode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryComponentTick.bCanEverTick = true;
	FireModeName = TEXT("Auto");
}

void UAutoFireMode::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	DispersionTick(DeltaTime);
	ReloadTick(DeltaTime);
	CooldownTick(DeltaTime);
	FireTick(DeltaTime);
}

void UAutoFireMode::OnAttached()
{
}

void UAutoFireMode::OnDropped()
{
}

void UAutoFireMode::StartUse()
{
}

void UAutoFireMode::StopUse()
{
}

void UAutoFireMode::OnTriggerPressed()
{
	if (Reloading && ReloadData->CanStopReload && HasAmmo()) {
		StopReload();
		Firing = true;
	}
}

void UAutoFireMode::OnTriggerReleased()
{
	if (Firing) {
		ShootingEnd();
	}
}

void UAutoFireMode::OnMovementStateChanged()
{
	UpdateDispersionInfo();
	if (WeaponInteractionModule->GetMovementState() == EMovementState::Sprint_State && Reloading)
		StopReload();
}

void UAutoFireMode::I_Reload()
{
	if (CurrentAmmo < MagazineSize) {
		TryReload();
	}
}


inline void ChangeDispersion(float& Param, const float& Value, FDispersionParams& DP)
{
	Param = FMath::Clamp(Param + Value, DP.Min, DP.Max);
}

void UAutoFireMode::DispersionTick(float DeltaTime)
{
	if (bApplyDispersion) {
		ChangeDispersion(DispersionRoll, -DeltaTime * (Dispersion.Roll.Reduction), Dispersion.Roll);

		ChangeDispersion(DispersionPitch, -DeltaTime * (Dispersion.Pitch.Reduction), Dispersion.Pitch);

		ChangeDispersion(DispersionYaw, -DeltaTime * (Dispersion.Yaw.Reduction), Dispersion.Yaw);
	}
}

void UAutoFireMode::DispersionRecoil()
{
	if (bApplyDispersion) {
		ChangeDispersion(DispersionRoll, Dispersion.Roll.Recoil, Dispersion.Roll);

		ChangeDispersion(DispersionPitch, Dispersion.Pitch.Recoil, Dispersion.Pitch);

		ChangeDispersion(DispersionYaw, Dispersion.Yaw.Recoil, Dispersion.Yaw);
	}
}

void UAutoFireMode::UpdateDispersionInfo()
{
	const FWeaponDispersion* FindResult = DispersionData->States.Find(WeaponInteractionModule->GetMovementState());
	if (FindResult == nullptr)
		Dispersion = FWeaponDispersion();
	else
		Dispersion = *FindResult;
}

void UAutoFireMode::ApplyDispersion(FRotator& ShotRotation)
{
	ShotRotation.Roll += FMath::RandRange(-DispersionRoll, DispersionRoll);
	ShotRotation.Pitch += FMath::RandRange(-DispersionPitch, DispersionPitch);
	ShotRotation.Yaw += FMath::RandRange(-DispersionYaw, DispersionYaw);
}


void UAutoFireMode::ReloadTick(float DeltaTime)
{
	if (Reloading) {
		if (ReloadTimer >= 0.0f) {
			if (WeaponInteractionModule)
			{
				OnReload.Broadcast();
			}
			ReloadTimer -= DeltaTime;
		}
		else {
			ReloadEnd();
		}
	}
}

void UAutoFireMode::TryReload()
{
	if (Reloading) return;

	if (WeaponInteractionModule->GetMovementState() == EMovementState::Sprint_State) return;

	if (CurrentAmmo < WeaponInteractionModule->GetAvailableAmmo(MagazineSize))
	{
		ReloadStart();
	}
}

void UAutoFireMode::ReloadStart()
{
	Reloading = true;
	
	UGameplayStatics::SpawnSoundAtLocation(
		GetWorld(),
		ReloadData->Sound,
		Weapon->GetActorLocation()
		);

	ReloadTimer = ReloadData->ReloadTime;

	OnReloadStart.Broadcast();
	WeaponInteractionModule->PlayAnimMontage(ReloadData->CharacterAnim.Anim, ReloadData->CharacterAnim.PlayRate);
	Weapon->PlayAnimMontage(ReloadData->WeaponAnim.Anim, ReloadData->WeaponAnim.PlayRate);
}

void UAutoFireMode::ReloadEnd()
{
	int AmmoFillAmount;
	if (ReloadData->ReloadFullMagazine) {
		AmmoFillAmount = MagazineSize - CurrentAmmo;
	}
	else {
		AmmoFillAmount = FMath::Min(ReloadData->PartialReloadSize, MagazineSize - CurrentAmmo);
	}
		CurrentAmmo += WeaponInteractionModule->GetAvailableAmmo(AmmoFillAmount);
		OnAmmoChange.Broadcast();
		
		if (CurrentAmmo < WeaponInteractionModule->GetAvailableAmmo(MagazineSize))
			ReloadStart();
		else
			StopReload();
}

void UAutoFireMode::StopReload()
{
	if(ReloadData->WeaponAnim.Anim)
		Weapon->StopAnimMontage(ReloadData->WeaponAnim.Anim);
	OnReloadEnd.Broadcast();
	Reloading = false;
	ReloadTimer = 0.0f;
}



void UAutoFireMode::CooldownTick(float DeltaTime)
{
	if (CoolsDown) {
		if (CooldownTimer >= 0.0f) {
			OnCooldown.Broadcast();
			CooldownTimer -= DeltaTime;
		}
		else {
			CooldownEnd();
		}
	}
}

void UAutoFireMode::CooldownStart()
{
	CoolsDown = true;

	OnCooldownStart.Broadcast();

	CooldownTimer = FireRate;
}

void UAutoFireMode::CooldownEnd()
{
	CoolsDown = false;
	OnCooldownEnd.Broadcast();
}


void UAutoFireMode::FireTick(float DeltaTime)
{
	if (!Reloading && !CoolsDown && Weapon->IsTriggerPressed()) {
		if (HasAmmo()) {
			ShootingStart();
		}
		else {
			if (!ReloadData->OnlyManualReload)
				TryReload();
		}
		if (Firing)
		{
			TryFire();
		}
	}
}

FRotator UAutoFireMode::GetSpawnRotation(const FVector& SpawnLocation) const
{
	FVector RelativeEndLocation;
	FVector CursorLocation{};
	WeaponInteractionModule->D_GetCursorLocation.ExecuteIfBound(CursorLocation);
	if ((SpawnLocation - CursorLocation).Size() > Weapon->SizeVectorToChangeShootDirectionLogic) {
		RelativeEndLocation = CursorLocation - SpawnLocation;
		RelativeEndLocation.Z = 0.0f;
	}
	else
		RelativeEndLocation = Weapon->GetActorForwardVector() * 1000.0f;

	// Debug
	{
		if (GetWorld()) {
			const FWeaponDebugSettings Debug;
			if (Debug.ShowDebug)
			{
				if (Debug.ShowDispersionCone)	//Dispersion Cone
				{
					DrawDebugCone(
					   GetWorld(),
					   SpawnLocation,
					   RelativeEndLocation.GetSafeNormal(),
					   Weapon->SizeVectorToChangeShootDirectionLogic,
					   DispersionYaw * PI / 180.0f,
					   DispersionPitch * PI / 180.0f,
					   32,
					   FColor::Emerald,
					   false,
					   2.5f,
					   (uint8)'\000',
					   1.0f
					   );
				}
				if (Debug.ShowWeaponDirection)	//direction weapon look
				{
					DrawDebugLine(
					   GetWorld(),
					   SpawnLocation,
					   SpawnLocation + Weapon->GetActorForwardVector() * 500.0f,
					   FColor::Cyan,
					   false,
					   5.f,
					   (uint8)'\000',
					   0.5f
					   );
				}
				if (Debug.ShowTraceToCursor)	//direction to cursor
				{
					DrawDebugLine(
					   GetWorld(),
					   SpawnLocation,
					   CursorLocation,
					   FColor::Red,
					   false,
					   5.f,
					   (uint8)'\000',
					   0.5f
					   );
				}
				if (Debug.ShowChangeShootDirectionLogicPoint)	//Logic Change point
				{
					DrawDebugSphere(
						GetWorld(),
						SpawnLocation + Weapon->GetActorForwardVector() * Weapon->SizeVectorToChangeShootDirectionLogic,
						10.f,
						8,
						FColor::Red,
						false,
						4.0f
						);
				}
			}
		}
	}
	RelativeEndLocation.Normalize();
	return RelativeEndLocation.Rotation();
}

void UAutoFireMode::ShootingStart()
{
	if (!Firing) {
		Firing = true;
		CooldownTimer = 0.0f;
		OnShootingStart.Broadcast();
	}
}

void UAutoFireMode::ShootingEnd()
{
	if (Firing) {
		Firing = false;
		OnShootingEnd.Broadcast();
	}
}

void UAutoFireMode::StopShooting()
{
	if(FireFXData->WeaponAnim.Anim)
		Weapon->StopAnimMontage(FireFXData->WeaponAnim.Anim);
	ShootingEnd();
}

void UAutoFireMode::TryFire()
{
	if(FireFXData->WeaponAnim.Anim) // Fire hase to be called from FireNotify
	{
		Weapon->PlayAnimMontage(FireFXData->WeaponAnim.Anim, FireFXData->WeaponAnim.PlayRate);
	}
	else
	{
		if(Weapon->ShootLocation)
			Fire(Weapon->ShootLocation->GetComponentLocation());
	}
	
	CooldownStart();
	

	auto& [Anim, PlayRate] = WeaponInteractionModule->GetMovementState() == EMovementState::Aim_State ? FireFXData->CharacterAimAnim : FireFXData->CharacterHipAnim;
	WeaponInteractionModule->PlayAnimMontage(Anim, PlayRate);
}

void UAutoFireMode::Fire(FVector SpawnLocation)
{
	const int& ProjectilesPerShot = ProjectilePerShot;
	const FRotator SpawnRotation = GetSpawnRotation(SpawnLocation);
	
	
	UGameplayStatics::SpawnSoundAtLocation(
		GetWorld(),
		FireFXData->Sound,
		SpawnLocation,
		SpawnRotation
		);
	
	UGameplayStatics::SpawnEmitterAtLocation(
		GetWorld(),
		FireFXData->Effect,
		SpawnLocation,
		SpawnRotation
		);
	
	for (int i = 0; i < ProjectilesPerShot; i++) {

		FRotator ModifiedSpawnRotation = SpawnRotation;
		if (bApplyDispersion)
		{
			ApplyDispersion(ModifiedSpawnRotation);
		}
		if (*ProjectileData->LogicClass) {
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = Weapon;
			SpawnParams.Instigator = Weapon->GetInstigator();
			AWeaponBase::FireProjectile(
				this,
				SpawnLocation,
				ModifiedSpawnRotation,
				SpawnParams,
				*ProjectileData
				);
		}
		else if(ProjectileData->TraceData)
		{
			FCollisionQueryParams QueryParams;
			AProjectileBase::OnHit(
				*ProjectileData,
				AWeaponBase::FireTrace(
					this,
					SpawnLocation,
					SpawnLocation + ModifiedSpawnRotation.Vector() * ProjectileData->TraceData->DistanceTrace,
					QueryParams
				),
				Weapon
			);
		}

		{// Debug
			const FWeaponDebugSettings Debug;
			if (Debug.ShowDebug && Debug.ShowFireDirection)
			{
				DrawDebugLine(
					GetWorld(),
					SpawnLocation,
					SpawnLocation + ModifiedSpawnRotation.Vector() * 2000.0f,
					FColor::Yellow,
					false,
					2.0f,
					(uint8)'\000',
					1.0f
					);
			}
		}
	}
	
	DispersionRecoil();

	UseAmmo(AmmoUsagePerShot);

	if (!HasAmmo()) {
		ShootingEnd();
		if(!ReloadData->OnlyManualReload)
			TryReload();
	}
}



bool UAutoFireMode::HasAmmo() const
{
	return (bool)CurrentAmmo;
}

void UAutoFireMode::UseAmmo(const int& Amount)
{
	CurrentAmmo -= WeaponInteractionModule->RemoveAmmo(Amount);
	OnAmmoChange.Broadcast();
}

void UAutoFireMode::UpdateAmmo()
{
	CurrentAmmo = WeaponInteractionModule->GetAvailableAmmo(CurrentAmmo);
}