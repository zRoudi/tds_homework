﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
// Core classes
#include "CoreMinimal.h"

// My classes
#include "TDS_HomeWork/Functions/Types.h"
#include "TDS_HomeWork/Weapon/Bases/FireModeBase.h"

// Modules

// Generated
#include "AutoFireMode.generated.h"

UCLASS(BlueprintType,Blueprintable)
class TDS_HOMEWORK_API UAutoFireMode : public UFireModeBase
{
	GENERATED_BODY()

public: // Public functions

	virtual void FillParams(UPARAM(ref) FParams& Params) const override;
	virtual bool SetPramsFromInfo() override;
	
	UAutoFireMode();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnAttached() override;
	virtual void OnDropped() override;
	
	virtual void StartUse() override;
	virtual void StopUse() override;
	
	virtual void OnTriggerPressed() override;
	virtual void OnTriggerReleased() override;

	virtual void OnMovementStateChanged() override;

	virtual void I_Reload() override;
	
public: // Public variables

	UPROPERTY(BlueprintAssignable)
	FNoParam OnAmmoChange;

	UPROPERTY(BlueprintAssignable)
	FNoParam OnShootingStart;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnFire;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnShootingEnd;

	UPROPERTY(BlueprintAssignable)
	FNoParam OnReloadStart;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnReload;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnReloadEnd;

	UPROPERTY(BlueprintAssignable)
	FNoParam OnCooldownStart;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnCooldown;
	UPROPERTY(BlueprintAssignable)
	FNoParam OnCooldownEnd;

protected: // Protected functions

protected: // Protected variables

private: // Private variables
	
	// Flags
	bool CoolsDown = false;
	bool Reloading = false;
	bool Firing = false;
	
	// Params
	bool bApplyDispersion = true;

	float FireRate = 0.5f;

	int MagazineSize = 10;
	int ProjectilePerShot = 1;
	int AmmoUsagePerShot = 1;

	// Timers
	float CooldownTimer = 0.0f;
	float ReloadTimer = 0.0f;

	// Fire
	int CurrentAmmo = 0;

	// Dispersion
	FWeaponDispersion Dispersion;
	float DispersionRoll = 0.0f;
	float DispersionPitch = 0.0f;
	float DispersionYaw = 0.0f;

private: // Private functions

	void DispersionTick(float DeltaTime);
	void DispersionRecoil();
	void UpdateDispersionInfo();
	void ApplyDispersion(FRotator& ShotRotation);
	FRotator GetSpawnRotation(const FVector& SpawnLocation) const;
	
	void ReloadTick(float DeltaTime);
	void TryReload();
	void ReloadStart();
	void ReloadEnd();
	void StopReload();
	
	void CooldownTick(float DeltaTime);
	void CooldownStart();
	void CooldownEnd();
	
	void FireTick(float DeltaTime);
	void ShootingStart();
	void ShootingEnd();
	void StopShooting();
	void TryFire();
	virtual void Fire(FVector SpawnLocation) override;
	
	bool HasAmmo() const;
	void UseAmmo(const int& Amount);
	void UpdateAmmo();
};
